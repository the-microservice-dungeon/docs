# The Microservice Dungeon Documentation

This is the place for documentation regarding the microservice dungeon project.\
All documentation should be done in this repository! \
Check it out on GitLab pages here: [the-microservice-dungeon.gitlab.io/docs](https://the-microservice-dungeon.gitlab.io/docs)!

The documentation is based [Hugo](https://gohugo.io/) with the [Docsy theme](https://www.docsy.dev/) and served by
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

## Running locally

**Prerequisites:**
* [Node.js](https://nodejs.org/en/)
* [Hugo](https://gohugo.io/getting-started/quick-start/)
or
* [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/)

Clone the repository (Note that Docsy is provided over a git submodule that needs to be initialized)
```sh
# HTTP
git clone --recurse-submodules https://gitlab.com/the-microservice-dungeon/docs.git
# SSH
git clone --recurse-submodules git@gitlab.com:the-microservice-dungeon/docs.git

cd ./docs
```
> Note: If you already cloned the repo run `git submodule update --init` fetch the Docsy submodule

### Using Hugo CLI

The Hugo CLI option offers dynamic re-rendering after change, which is kind of a prerequisite for a smooth
development workflow in case you want to change something. After the repository has been cloned, you would need
to install dependencies from node:
```sh
npm ci
```
Now you can start the hugo server by using
```sh
hugo serve
```

The site is now available at [http://localhost:1313/](http://localhost:1313/)

### Using Docker

Here we have no dynamic re-rendering upon change. Therefore you would need to restart the container after each
edit. This is not recommended for development, but can be used for testing purposes.

Simply run
```sh
# Docker Desktop
docker compose up
# docker-compose
docker-compose up
```

The site is now available at [http://localhost:1313/](http://localhost:1313/)

### Testing the Site Locally with `htmltest`

If you don't want the build to fail, you need to run the `htmltest` tool locally. This checks for broken links; any
violation leads to a build failure. To run the tool, use the following command:

```sh
hugo # without any parameters, this just builds the site to the `public` directory
htmltest -c .htmltest.yml public
```

(Of course you need to have htmltest installed, see [htmltest repo](https://github.com/wjdp/htmltest) for details -
it is just a single binary that you put somewhere in your PATH.)


### Troubleshooting Hugo Build Issues

As you run the website locally, you may run into the following error:

```
➜ hugo server

INFO 2021/01/21 21:07:55 Using config file:
Building sites … INFO 2021/01/21 21:07:55 syncing static files to /
Built in 288 ms
Error: Error building site: TOCSS: failed to transform "scss/main.scss" (text/x-scss): resource "scss/scss/main.scss_9fadf33d895a46083cdd64396b57ef68" not found in file cache
```

This error occurs if you have not installed the extended version of Hugo.
See this [section](https://www.docsy.dev/docs/get-started/docsy-as-module/installation-prerequisites/#install-hugo) of the user guide for instructions on how to install Hugo.

Or you may encounter the following error:

```
➜ hugo server

Error: failed to download modules: binary with name "go" not found
```

This error occurs if you have not installed the `go` programming language on your system.
See this [section](https://www.docsy.dev/docs/get-started/docsy-as-module/installation-prerequisites/#install-go-language) of the user guide for instructions on how to install `go`.


## Adding Content to the documentation

This guide is just a quick overview, refer to the [Docsy documentation](https://www.docsy.dev/docs/) for more details.

The whole content is located under the `content/en` location. Refer to the structure there to get an overview.
You can simply add markdown files with a simple [Frontmatter](https://www.docsy.dev/docs/adding-content/content/).

Refer to the [Linking tips](https://www.docsy.dev/docs/best-practices/site-guidance/#linking) on how to properly link content if you're experiencing troubles with it. In the pipeline runs a HTML Tester to test on valid links and more things to guide you there.

### Regarding AsyncAPI

Unfourtunally the [AsyncAPI](https://github.com/asyncapi/asyncapi-react) component does to the date of writing not support other spec versions than **2.0.0**. Please make sure that your specifications adhere **2.0.0** if possible. IF there are reasons on using another version we need to implement a workflow for using the [AsyncAPI Generator](https://github.com/asyncapi/generator) and generate HTML/Markdown dynamically.
Hoping that we don't require any of the new features and that AsyncAPI **3.0.0** will be supported again by the component.
