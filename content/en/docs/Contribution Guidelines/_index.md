---
title: "Contribution Guidelines"
linkTitle: "Contribution Guidelines"
weight: 8
description: ""
---

Contributions to **Core Services** and **Player Skeletons** must go through a review process before they can be accepted
and merged.

This process is described below, followed by a list of the most important coding practices that are taken into account
during the review. It is therefore recommended to be aware of those when submitting new features.


## Review and Merge Process

The overall goal of the review process is to ensure compliance with standards and to check for errors -
four eye principle.

Each new feature is developed on a separate feature branch. When it's ready, a merge request is created and shared. A
supervisor of the MSD then looks at the request and makes comments where improvement is needed, which must be
incorporated by the developer afterward. This sequence may repeat several times. As soon as the approval is given, the
DoD can be started - Definition of Done, a term used in scrum to define the finale state an increment must reach in
order to be completed.

The following outlines all necessary steps in detail:

### Development
Development should take place on a separate branch. As changes in multiple repositories will likely be necessary,
the following naming policy must be used to ensure consistency.

```text
{first letter first name}{first two letters last name, first letter capital}/{feature name in kebab case}

(1) dKo/log-aggregation
(2) cLo/real-time-msd
```

### Creating a merge request
After development is completed, a merge request can be created.


- **Update your branch**<br/>
  Make sure your branch is up to date by merging the latest changes from 'main' beforehand.
  <br/><br/>

- **Document your feature**<br/>
  Each feature should be documented to some extent, either through a task or an issue with a specific requirement, an
  update to the microservice dungeon documentation or through a short summary at the top of your merge request.
  _Please don't comment on specific lines of your code in your request._
  <br/><br/>

- **Share your request**<br/>
  Finally you can share your request(s) on discord to submit it for review.

### Review
A reviewer will look at your request after it has been submitted and will make comments where improvement is needed. No
further changes should be made at this point unless absolutely necessary, in which case your reviewer should be
notified.

### Incorporating the feedback
Once your review is done, you can start incorporating your feedback. Depending on the complexity and severity of the
changes required, another review cycle may be advised.

### Completing the Definition of Done
As soon as all threads are closed and the approval is given, you may begin the final steps in order to complete
your request.

- **Update your branch**<br/>
  First, update your branch once again with the latest changes from 'main'.
  <br/><br/>

- **Run automated tests**<br/>
  After that, run all automated tests locally - this includes integration tests.
  <br/><br/>

- **Manual test**<br/>
  Check that the basic functionality of the MSD still works without any errors using the local development environment -
  especially your newly developed feature.
  <br/><br/>

- **Merge**<br/>
  Finally, you can merge your request. Make sure to delete your request and branch afterward.
