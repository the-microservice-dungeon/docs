---
title: "Logging"
linkTitle: "Logging"
weight: 1
description: >
  Everything around our logging platform
---

We are using a part of the ELK stack for accessing our application logs.
To be precise we're using Filebeat to ship the logs from our Kubernetes
nodes to Elasticsearch. And Kibana to visualize the logs in Elasticsearch.

So the marked parts are those we are using from the ELK stack:

<img src="/images/logging/elk-stack.png" width="50%" height="50%" alt="ELK Stack">

## How to access your application logs?
To access your Logs via Kibana, you have to be connected with the TH Köln VPN.

Therefore please have a look at this reference: https://www.th-koeln.de/mam/downloads/deutsch/hochschule/organisation/vpn-settingup.pdf


### Discover

Kibana is available at this URL: http://kibana.dungeon-space.de/

If you aren't connected with the VPN you will get an `403 Forbidden` error by nginx.

If your are connected and open Kibana, the first thing you should do to access your logs,
is to navigate to the `Discover` view.

![Home](/images/logging/home.png)

![Side-Menu](/images/logging/side-menu.png)


### Documents

Now you can experiment a lot to get the perfect view on the logs you want to see.
In the center you see the log documents, with all the metadata of Kubernetes & Filebeat.
A document is nothing else than a JSON object.

![Documents](/images/logging/documents.png)

### Time range

On the top right you can set the range of time.

![Time](/images/logging/time.png)

### Search

With the search bar you can filter for specific fields. Of course you can combine your queries with
logical operators. Feel free to experiment.

![Query](/images/logging/search.png)

### Filter

To only show fields that are relevant for you, you can filter them too:

![Filter](/images/logging/filter.png)

So have fun while analyzing your logs!

### Stream

If you want to stream your logs live, you can use the Observability -> Stream view:

![Stream](/images/logging/stream.png)


