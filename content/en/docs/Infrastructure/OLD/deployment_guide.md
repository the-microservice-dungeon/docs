---
title: "Deployment Guide"
linkTitle: "Deployment Guide"
weight: 1
description: >
  Deployment Guide
---

# How to ship your application to Kubernetes
This guide will explain the deployment of you Helm Chart, on our Kubernetes Cluster.

## Step 1: Push your Helm Chart to the Gitlab Registry
For this step you obviously need a Helm Chart, but I assume you already have one.
So to push your Helm Chart to the Gitlab package registry you have to setup the following
things in the `.gitlab-ci.yml` configuration of your Chart:

##### 1. Include the helm-package-publish job
```yaml
include:
  - project: "the-microservice-dungeon/devops-team/common-ci-cd"
    ref: "main"
    file: "helm/package-publish.yaml"
...
```
##### 2. Add the helm stage to your stages:
```yaml
...
stages:
  - helm
...
```
##### 3. Add the mandatory variables:
```yaml
...
variables:
  PATH_TO_CHART: "helm-chart"
  CHART_NAME: "player-hackschnitzel"
...
```
  * `PATH_TO_CHART`: references the directory of your Helm Chart in your repository
  * `CHART_NAME`: the name of your Helm Chart
##### 4. Overwrite the job rules:
```yaml
...
helm-package-publish:
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - ${PATH_TO_CHART}/**/*
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: always
...
```
So now the job gets triggered, if you change a file of your Helm Chart on your default branch
or if you run the pipeline manually (in your repo navigate to CI/CD -> Pipelines -> Run Pipeline).

Our full configuration now should look like this (you will probably have more jobs and stages
if you build and test your app):
```yaml
include:
  - project: "the-microservice-dungeon/devops-team/common-ci-cd"
    ref: "main"
    file: "helm/package-publish.yaml"

stages:
  - helm

variables:
  PATH_TO_CHART: "helm-chart"
  CHART_NAME: "player-hackschnitzel"

helm-package-publish:
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - ${PATH_TO_CHART}/**/*
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: always
```

If you run your pipeline now, you should see your packaged Helm Chart in the
Gitlab package registry: https://gitlab.com/groups/the-microservice-dungeon/-/packages

If you want to deploy a new version of your Chart, make sure to update the version in your `Chart.yaml`.

The complete example can be found here: https://gitlab.com/the-microservice-dungeon/player-hackschnitzel

## Step 2: Add the HelmRelease to FluxResources
The next step is to add a HelmRelease in the FluxResources Repo: https://gitlab.com/the-microservice-dungeon/devops-team/FluxResources/-/tree/master/

Please add you HelmRelease file at the following path: `cluster/${YOUR_NAMESPACE}/${YOUR_APP_NAME}.yaml`.

If you don't know the name of your namespace, feel free to ask us on Discord.

The HelmRelease Yaml file you add should look like this:
```yaml
apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: player-hackschnitzel
  namespace: player-hackschnitzel
spec:
  chart:
    spec:
      chart: player-hackschnitzel
      reconcileStrategy: ChartVersion
      sourceRef:
        kind: HelmRepository
        name: dungeon
        namespace: flux-system
  interval: 1m0s
  releaseName: player-hackschnitzel
  targetNamespace: player-hackschnitzel

```
Please change the following values:
* `metadata.name`: Your application name
* `metadata.namespace`: Your namespace
* `spec.chart.spec.chart`: The name of your chart (set in your `.gitlab-ci.yml`)
* `spec.releaseName`: Your applcation name
* `targetNamespace`: Your namespace
Optional:
Add `spec.values` to override specific keys of your values.yaml
```
...
spec:
  values:
    image: ...
    tag: ...
    env:
      foo: bar
    ...
```

## Step 3: Log into Kubernetes to see the result & debug
For this step a (Cluster)RoleBinding must be created by an K8s admin for you.
If you're a member of the current MSD Iteration, this should already be the case.
To log into the Kubernetes Cluster via kubectl you have to follow these instructions:

##### 1. Install kubectl
https://kubernetes.io/docs/tasks/tools/#kubectl

##### 2. Install krew (kubectl package manager)
https://krew.sigs.k8s.io/docs/user-guide/setup/install/

##### 3. Install kubelogin
```zsh
# Krew (macOS, Linux, Windows and ARM)
kubectl krew install oidc-login
```

##### 4. Get the kuebctl config
Copy the kuebctl config file from here: https://gitlab.com/the-microservice-dungeon/devops-team/kube-cluster-config/-/blob/master/kube-config/config

Paste it to `~/.kube/config` on your host.

##### 5. Use kubectl
If you run your first command now, e.g. `kubectl get namespaces`, you should be forwarded to
`http://localhost:8000` in your browser and have to confirm that our Gitlab OAuth2 Application
can access your Gitlab account (to get read claims, like your username).

After confirming this you are authenticated and should be able to use kubectl.

If you miss any components of you Helm Chart you can check if the deployment was successful
by looking at the HelmRelease:

```zsh
kubectl get helmreleases.helm.toolkit.fluxcd.io
NAME           AGE   READY   STATUS
player-monte   52m   False   Helm install failed: parse error at (player-monte/templates/deployment.yaml:21): unexpected {{else}}
```

If you need more information about your HelmRelease you can use desribe:

```zsh
kubectl describe helmreleases.helm.toolkit.fluxcd.io player-monte
...
```

## Finished
You're finished and should be able to see and debug your deployment on the cluster.

If you have any questions, please feel free to ask in the DevOps Discord Channel.
Please let us know if anything in this document is unclear so we can improve the guide.
