---
title: "Overview"
linkTitle: "Overview"
weight: 1
description: >
  Basics Overview for k8s
---

<!-- TOC -->
* [General](#general)
* [What is Kubernetes](#what-is-kubernetes)
* [What is Kubectl](#what-is-kubectl)
* [What is Helm](#what-is-helm)
* [What is OpenTofu / Terraform](#what-is-opentofu--terraform)
* [What is Rancher](#what-is-rancher)
* [What is Harvester](#what-is-harvester)
* [What are k3s and RKE2](#what-are-k3s-and-rke2)
* [What is Rancher Fleet](#what-is-rancher-fleet)
* [What is Nginx and Certbot](#what-is-nginx-and-certbot)
<!-- TOC -->

---

# General
```text
Below are some links that new members of the DevOps team should check out
(Not mandatory, but highly recommended)
```
Links:
- [YT → TechWorld with Nana → Kuberntetes Full Course](https://www.youtube.com/watch?v=X48VuDVv0do)

---

# What is Kubernetes
```text
Kubernetes is one of the applications for orchestrating containers.
This makes it possible to handle services of different kinds.
Besides services, it also manages disk storage, networks, and much more.
```
More on Kubernetes:
- [kubernetes](https://kubernetes.io/)
- [docs](https://kubernetes.io/docs/home/)
- [Get Started](https://kubernetes.io/docs/tutorials/kubernetes-basics/)
- [concepts](https://kubernetes.io/docs/concepts/overview/)

---

# What is Kubectl
```text
Kubectl is the management application to interact with the resources of Kubernetes clusters.
With Kubectl, "yaml files" can be deployed to the cluster.
Additionally, resource consumption can be managed, or logs can be viewed.
Furthermore, the status of various services can be viewed.
```
- More on kubectl: [kubectl](https://kubernetes.io/docs/reference/kubectl/)

---

# What is Helm
```text
Helm is an easy way to interact with an existing Kubernetes cluster.
With Kubectl, only one "yaml file" can be installed at a time, or each file must be specified individually.

With Helm, so-called "Helm Charts" are created,
which contain all the required resources like deployments, services, etc.

Another advantage of Helm Charts is that the values of variables can be defined in the "values.yaml file"
and then referenced in the resources upon invocation.

Hence, Helm offers a significantly better way to install, update, or delete resources on the cluster.

Other functions of kubectl, however, are not taken over by Helm.
```
More on Helm:
- [helm](https://helm.sh/)
- [docs](https://helm.sh/docs/)

---

# What is OpenTofu / Terraform
```text
Terraform, or the fork OpenTofu, is one of the ways to automate infrastructure.
These applications are summarized under "Infrastructure as Code (IaC)".

Here, "*.tf files" are created, and a plan is generated. The plan is then written into a "state file".

Subsequently, the plan is realized through an apply. Here, the state of the target is compared with the
state in the "state file", and differences in the target are adjusted to the "state file".

Thus, the "*.tf files" specify what the "state" looks like, and Terraform automatically
establishes the state in the target based on the "state".
```

- More on Terraform: [docs](https://developer.hashicorp.com/terraform?product_intent=terraform)
- More on OpenTofu: [docs](https://opentofu.org/docs/)

---

# What is Rancher
```text
Rancher is a software from SUSE Linux and is used to manage Kubernetes clusters.
It provides a graphical user interface via the web browser.

With Rancher, new clusters, networks, rules, disk storage, users,
and much more can be created and managed.

Simply put: The management tool for cluster administration
```

- More on Rancher v2.8: [docs](https://ranchermanager.docs.rancher.com/v2.8/)

---

# What is Harvester
```text
Harvester is also an application from SUSE Linux. With Harvester, virtual
machines can be created.

Harvester can be integrated as a cloud provider in Rancher. This allows new Kubernetes clusters to be created on the
Harvester via Rancher's management.
```

- More on Harvester (v1.2): [docs](https://docs.harvesterhci.io/v1.2/)

---

# What are k3s and RKE2
```text
K3S and RKE2 are both applications from SUSE Linux and represent applications for
installing Kubernetes clusters on bare metal.
```

- More on k3s: [k3s](https://docs.k3s.io/)
- More on RKE2: [rke2](https://docs.rke2.io/)

---

# What is Rancher Fleet
```text
Rancher Fleet is the built-in "GitOps Tool" of Rancher. It automates the process
of deploying on the clusters. It is comparable to ArgoCD or Flux.

A "fleet.yaml" must be placed in the GitRepo. If there are changes to the Repo, then the deployment of the service updates
```

- More on Fleet: [fleet](https://fleet.rancher.io/)

---

# What is Nginx and Certbot
```text
Nginx bietet viele Möglichkeiten, im MSD wird Nginx zur Weiterleitung der eingehenden
URL an unsere VM genutzt.

Um "HTTPS" zu ermöglichen verwenden wird zusätzlich den Certbot, um unsere Verbindung
mit Zertifikaten auszustatten.
```

- More on Nginx: [docs](https://nginx.org/en/docs/)
- More in Certbot:
  - [certbot](https://eff-certbot.readthedocs.io/en/stable/intro.html)
  - [letsencrypt](https://letsencrypt.org/docs/)

---


