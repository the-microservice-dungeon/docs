---
title: "Conventions"
linkTitle: "Conventions"
weight: 2
description: >
  Basic Conventions for k8s, like helm charts, variable names and so on
---

<!-- TOC -->
* [Helm Charts](#helm-charts)
  * [Variables](#variables)
  * [Chart.yaml](#chartyaml)
  * [values.yaml](#valuesyaml)
  * [fleet.yaml](#fleetyaml)
  * [deployment.yaml](#deploymentyaml)
  * [service.yaml](#serviceyaml)
  * [ingress.yaml](#ingressyaml)
  * [loadbalancer.yaml](#loadbalanceryaml)
<!-- TOC -->

---

# Helm Charts

## Variables

| Type | Variable | Value |
|------|----------|-------|
|      |          |       |
|      |          |       |
|      |          |       |

---

## Chart.yaml
```yaml

```

---

## values.yaml
```yaml

```

---

## fleet.yaml
```yaml

```

---

## deployment.yaml
```yaml

```
---

## service.yaml
```yaml

```

---

## ingress.yaml
```yaml

```

---

## loadbalancer.yaml
```yaml

```

---
