---
title: "Minikube Core Service Interaction"
linkTitle: "Minikube Core Service Interaction"
weight: 3
description: >
 Minikube Core Service Interaction

---

# local-environment-minikube Core Service Interaction

The `./core-service-interaction.sh` is your way to install, delete or update core-services \
enter `./core-service-interaction.sh -h` or `./core-service-interaction.sh --help` to see help with available commands \

If you like to interact more with your cluster, please check a look at kubectl documentation \
(tip `kubectl [command]` for interaction that is not covered by this site )

---

#### current core-services for interaction
1. robot
2. map
3. trading
4. game
5. gamelog

---

## Commands

For every command is a short, and a long version available

You can combine every command (except ALL and the non core-service commend) for multiple actions at same time


RUN `./core-service-interaction.sh` and add a command

| Command                  | short | long          |
|--------------------------|-------|---------------|
| help                     | -h    | --help        |
| update core service repo | -ur   | --update-repo |


| Service | Command<br/> Version | Install                | Update                     | Delete                      |
|---------|----------------------|------------------------|----------------------------|-----------------------------|
| ALL     | short<br/> long      | -ase<br/> --allservice | -uase<br/> --up-allservice | -dase<br/> --del-allservice |
| robot   | short<br/> long      | -ro<br/> --robot       | -uro<br/> --up-robot       | -dro<br/> --del-robot       |
| game    | short<br/> long      | -ga<br/> --game        | -uga<br/> --up-game        | -dga<br/> --del-game        |
| trading | short<br/> long      | -tr<br/> --trading     | -utr<br/> -- up-trading    | -dtr<br/> --del-trading     |
| map     | short<br/> long      | -ma<br/> ..map         | -uma<br/>  --up-map        | -dma<br/>  --del-map        |
| gamelog | short<br/> long      | -gl<br/>  --gamelog    | -ugl<br/>  --up-gamelog    | -dgl<br/> --del-gamelog     |


---

#### Examples

The `./core-service-interaction.sh -dga -ga` remove the game service from cluster and install game after this
