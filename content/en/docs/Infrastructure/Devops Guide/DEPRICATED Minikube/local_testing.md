---
title: "Minikube Local Testing"
linkTitle: "Minikube Local Testing"
weight: 4
description: >
  Minikube Local Testing
---

# Local Environment Minikube Local Testing

This site covers everything for our Repo `local-environment-minikube`

Goal of this Project is, to run a single script and everything is ready to use.

**Do this if you like to test your deployment locally before upload it on MSD gitlab**

**Normally your run your code from the IDE**

---

## Prepare Docker

#### on Linux

Open a Terminal and find your local IP-Address with `ip a` NOT localhost, recommended your IP-Address over LAN (WLAN is possible)

create or edit the `/etc/docker/daemon.json`

add the following to the daemon.json: ===> The UP-Address is your IP-Address from above
`````
{
        "insecure-registries": ["192.168.188.121:5000"]
}

`````
now restart your docker

#### on Windows

Open Powershell and find your WSL IP-Address with `ipconfig` NOT localhost, **important** it must the WSL Client IP Address

In Docker Desktop `Settings -> Docker Engine`

past the following into the text field: ===> The UP-Address is your IP-Address from above
`````
{
        "insecure-registries": ["192.168.188.121:5000"]
}

`````
now restart your docker

![Docker Desktop WSL](/images/minikube/DockerDesktop02.png)


## Install Cluster

(Windows User in WSL Linux)

Run `./firstBuildCluster.sh -ins` or `./firstBuildCluster.sh -ins --allservice` if your like to install all core services at same time

now the skript ask you to enter your IP-Address (see above this IP-Address) and the chosen Port
enter both, and the automated installation will start.


## Build Docker image

#### on Windows
The chosen service must be placed that you can run it into WSL

Example: player-hackschnitzel

![WSL Path](/images/minikube/wsl_path_for_deploy.png)


#### on Both

Enter the Directory of the service with a terminal (for windows, in the WSL Linux)

make sure you have a `Dockerfile` int is needed for this Guide

change the minikube Environment to docker environment: `eval $(minikube docker-env) `

now install the local registry: `sudo docker run -d -p 5000:5000 --restart=always --name registry registry:2 ` the port is the same as your port specified by running cluster build see above

now build your Docker Image: `sudo docker build --no-cache -t player-hackschnitzel:v0.1 . `

aft build, tag your Image to the registry: `sudo docker tag player-hackschnitzel:v0.1 192.168.188.121:5000/player-hackschnitzel:v0.1  `
the IP-Address must be the same as that what your entered before

now push your Image into your registry: `sudo docker push 192.168.188.121:5000/player-hackschnitzel:v0.1`

Now we have our Image in our private registry

## very basic helm chart

#### create basic helm charts
The following YAML-Files didn't follow the way, how helm-charts should be built
normally you have a `values` file with references into the yaml-files

![package structure helm chart](/images/minikube/package_structure_helmchart.png)

##### 1. deployment
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: player-hackschnitzel-deployment
  namespace: player-hackschnitzel
  labels:
    app: player-hackschnitzel
spec:
  replicas: 1
  selector:
    matchLabels:
      app: player-hackschnitzel
  template:
    metadata:
      labels:
        app: player-hackschnitzel
    spec:
      containers:
      - name: player-hackschnitzel
        image: 192.168.188.121:5000/player-hackschnitzel:v0.1
        imagePullPolicy: IfNotPresent
        resources:
          requests:
            memory: "256Mi"
            cpu: "500m"
          limits:
            memory: "1G"
            cpu: "1000m"
      volumes:
      - name: logs-volume
        emptyDir:
          sizeLimit: 500Mi
...
```
##### 2. service
```yaml
apiVersion: v1
kind: Service
metadata:
  name: player-hackschnitzel-service
spec:
  selector:
    app: player-hackschnitzel
  ports:
    - protocol: TCP
      port: 8000
      targetPort: 8000


---

apiVersion: v1
kind: Service
metadata:
  name: player-hackschnitzel-service-lb
spec:
  type: LoadBalancer
  selector:
    app: player-hackschnitzel
  ports:
    - protocol: TCP
      port: 38888
      targetPort: 8000

...
```

#### interaction

open a new terminal (Windows in WSL)

run `helm install player-hackschnitzel helm-chart --namespace player-hackschnitzel --create-namespace  ` for install deployment

run `helm uninstall -n player-hackschnitzel player-hackschnitzel ` to remove deployment

run `helm upgrade player-hackschnitzel helm-chart --namespace player-hackschnitzel` for update the deployment


When your have another local image, change image in deployment and run helm update

When your change something inside the helmchart, e.g. the loadbalancer port run helm update
