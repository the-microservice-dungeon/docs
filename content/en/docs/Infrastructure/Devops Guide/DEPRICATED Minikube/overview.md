---
title: "Minikube Overview"
linkTitle: "Minikube Overview"
weight: 1
description: >
  Minikube Overview
---

# Local Environment Minikube Overview

This site covers everything for our Repo [[local-environment-minikube](https://gitlab.com/the-microservice-dungeon/devops-team/local-environment-minikube)]

Goal of this Project is, to run a single script and everything is ready to use.



#### Infrastructure

Using the `installInfrastructure.sh` script.

1. add needed repos
2. install
   1. cert-manager (certificate controller)
   2. prometheus-stack (monitoring system)
   3. redpanda-operator (streaming data platform, our kafka replace)
   4. rabbitmq (message broker)
   5. kafka-rabbitmq-connector
3. install `one-node-cluster.yml` on redpanda (config for redpanda)

THIS SHOULD ONLY EDIT BY DEVOPS TEAM


#### First Build Cluster

Using the `firstBuildCluster.sh` script.

1. install automate `installInfrastructure.sh`
2. install chosen core service
   1. game
   2. map
   3. robot
   4. trading
   5. gamelog
3. allow config for private (insecure) registry

THIS SHOULD ONLY EDIT BY DEVOPS TEAM

Refer our [[First Build Cluster](../install_cluster)]



#### Core Service Interaction

Using the `core-service-interaction.sh` script.

The interaction for install, update and delete the core-services
1. game
2. map
3. robot
4. trading
5. gamelog

Refer our [[Core Service Interaction](../core_service_interaction)]



#### Delete Cluster

Using the `deleteCluster.sh` script.

This stop the Custer and delete everything

Run `./deleteCluster.sh`
