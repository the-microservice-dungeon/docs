---
title: "Minikube install Cluster"
linkTitle: "Minikube install Cluster"
weight: 2
description: >
  Minikube install Cluster
---

# Local Environment Minikube
## First Build Cluster

This site covers everything for the `firstBuildCluster.sh` script in our Repo [[local-environment-minikube](https://gitlab.com/the-microservice-dungeon/devops-team/local-environment-minikube/)]



#### Preparation

**_On Windows: Refer to our [Windows WSL](../windows_wsl/)_**

When you using linux, or after prepare your Windows WSL machine, you can do the following steps
(Windows users run this in WSL Linux).

Needed programms should be in system package repos, if not, see following links.
When one of these is missing the script notice you about it, and didn't execute.

1. Install needed programms
   - git
   - [[minikube](https://kubernetes.io/de/docs/tasks/tools/install-minikube/)]
   - [[kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)]
   - [[helm](https://helm.sh/docs/intro/install/)]

2. Config minikube
   - `minikube config set memory 8192`
   - `minikube config set cpus 6`
   - Driver
     - on Windows, minikube chose automated Docker Desktop from Windows Host. Do not specify a driver.
     - on Linux, the driver for Docker is automated chosen

3. Make sure, thar the following files are executable
   - firstBuildCluster.sh
   - installInfrastructure.sh
   - core-service-interaction.sh
   - deleteCluster.sh

You can run `chmod +x *.sh`, after this, all .sh files in the directory are executable

---

### Running `firstBuildCluster.sh` script

Run `./firstBuildCluster.sh` that install full infrastructure, and all core services. After this, you can use the cluster.

---

### Only Infrastructure

Run `./firstBuildCluster.sh --noservice` that install full infrastructure, and NO core services.
Use `core-service-interaction.sh` for install needed core-services later.

---

### Available Commands
Run `./firstBuildCluster.sh --help` or `./firstBuildCluster.sh -h`

![Commands](../../../../../../static/images/minikube/first_install_help.png)

Refer our [[Install for local testing](../local_testing/)]

---

### Local Testing / local (insecure) registry



---
### Start and Stop

Run `minikube stop` to stop the cluster
Run `minikube start` to start the cluster
This start the existing cluster with all existing infrastructure and core-services

---

## Ports for Services

Refer our [[Service external access](../external_access_to_services_in_minikube/)]



## Interaction with Core Services

Refer our [[Core Service Interaction](../core_service_interaction/)]


