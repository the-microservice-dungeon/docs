---
title: "Minikube access to services"
linkTitle: "Minikube access to services"
weight: 4
description: >
  Minikube access to services
---

# Local Environment Minikube service access

Ports for Services

| service                            | port  | nodePort |
|------------------------------------|-------|----------|
| gamelog                            | 30001 | 30001    |
| game                               | 30002 | 30002    |
| map                                | 30003 | 30003    |
| robot                              | 30004 | 30004    |
| trading                            | 30005 | 30005    |
| rabbitmq internal                  | 5672  | 30006    |
| rabbitmq external <br/> management | 15672 | 30007    |

---



### On Windows with WSL2

Enter `minikube tunnel` in an new WSL2 Linux (Ubuntu) Terminal.

The ports will be exposed for IP-address: `127.0.0.1` on Windows host system.

The access is `127.0.0.1:port`

### On Linux

The `firstBuildCluster.sh` will chose (automated) docker as standard driver.

Minikube is configured to export Ports to static IP-address `10.10.10.10`

after entering `minikube service list` you see `10.10.10.10:nodePort`

no `minikube tunnel` needed

---

Be aware, that you can still use `minikube tunnel` on Linux,
but the IP-addresses for the services will be a random IP-address from the 10th-net

For more information please refer to [Minikube accessing apps](https://minikube.sigs.k8s.io/docs/handbook/accessing/#loadbalancer-access)

---

### For rabbitmq

External access for rabbitmq management (access over webbrowser) is Port 15672,
but you can't access this port on Windows WSL2, so a loadbalancer is in use to give you access on this port `15672`

The same goes for the internal use of Rabbitmq over port 5672 this will be also exposed for your player service over a loadbalancer at port `5672`.

---

#### On Windows WSL


| Service           | IP:PORT         | replacement for                     |
|-------------------|-----------------|-------------------------------------|
| rabbitmq internal | 127.0.0.1:5672  | rabbitmq-service -> ClusterIP:5672  |
| rabbitmq external | 127.0.0.1:15672 | rabbitmq-service -> ClusterIP:15672 |

As mentioned above this is the only way to access the rabbitmq management on your windows host webbrowser
And the only way to interact internally with your player outside on the WSL


---

#### On Linux

| Service           | IP:PORT           | replacement for                     |
|-------------------|-------------------|-------------------------------------|
| rabbitmq internal | 10.10.10.10:30006 | rabbitmq-service -> ClusterIP:5672  |
| rabbitmq external | 10.10.10.10:30007 | rabbitmq-service -> ClusterIP:15672 |


On Linux as mentioned before, we use nodePorts

But you can still use the rabbitmq-service to access the internal and management


You can run `kubectl get service --namespace rabbitmq`

Choose `ClusterIP from rabbitmq-service` (not rabbitmq-*-lb).

After `minikube tunnel` you can access the management over this `ClusterIP:15672`. This is the same as `10.10.10.10:30007`

For internal Player testing you can use the same ClusterIP, with internal Port `ClusterIP:5672`. This is the same as `10.10.10.10:30006`

Be aware that the ClusterIP will change every time your build the cluster from bottom, but the minikube IP will be 10.10.10.10
(minikube stop and minikube start, the ClusterIP will not change)


