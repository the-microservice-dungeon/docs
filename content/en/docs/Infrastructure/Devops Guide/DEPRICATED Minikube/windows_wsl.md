---
title: "Minikube on Windows WSL"
linkTitle: "Minikube on Windows WSL"
weight: 2
description: >
 Minikube on Windows WSL
---


***

__For WSL2 \
[WSL Overview](https://learn.microsoft.com/en-us/windows/wsl/) \
[WSL in MS-Store Info](https://devblogs.microsoft.com/commandline/the-windows-subsystem-for-linux-in-the-microsoft-store-is-now-generally-available-on-windows-10-and-11/)__

---

<!-- TOC -->
* [Preparation](#preparation)
  * [In Windows Features](#in-windows-features)
  * [For WSL](#for-wsl)
  * [For Docker](#for-docker)
* [WSL config in windows (global config)](#wsl-config-in-windows-global-config)
  * [first time installation of Ubuntu](#first-time-installation-of-ubuntu)
  * [config in Ubuntu](#config-in-ubuntu)
* [Nice to Have](#nice-to-have)
<!-- TOC -->

# Preparation

## In Windows Features

1. activate feature „WSL“ in Windows features
2. activate feature „Hypervisor“ in Windows features


![windowsFeatures](/images/minikube/windows_features.png)

## For WSL
1. load WSL from MS store
2. load Ubuntu 22.04.1 LTS from MS store


## For Docker

1. install Docker Desktop
2. activate in Docker Desktop under `Settings -> Resources ` the Ubuntu Distro


![Docker Desktop WSL2](/images/minikube/DockerDesktop01.png)


# WSL config in windows (global config)


**Hint: Please enable show hidden files and file extension, so you can create `.wslconfig` and not create mistakenly `.wslconfig.txt`**


edit / create `C:\Users\<UserName>\.wslconfig` insert following

````
# settings for all Linux distros
[wsl2]

# Limit for VM memory un GB or MB
memory=10GB

# Set VM virtual processors
processors=7

# allow nested virtualization
nestedVirtualization=true
````

run in powershell `wsl.exe --shutdown` as admin (shutdown wsl)

run in powershell `wsl.exe -l -v` as admin  (see stopped distros)

this above give your wsl access to 10gb ram, 7 cpu cores

---

## first time installation of Ubuntu

During the initial installation or first startup of WSL Ubuntu,
a new user will be created. Enter a new username, then set a password.
Note that, as is typical with Unix systems, the password input will not be visible.
After this, this user will be used as the default user for Ubuntu.

---

## config in Ubuntu

1. run Ubuntu installation
   1. run following for better network performance
      1. `sudo rm /etc/resolv.conf`
      2. `sudo bash -c 'echo "nameserver 8.8.8.8" > /etc/resolv.conf'`
      3. `sudo bash -c 'echo "[network]" > /etc/wsl.conf'`
      4. `sudo bash -c 'echo "generateResolvConf = false" >> /etc/wsl.conf'`
      5. `sudo chattr +i /etc/resolv.conf`
   2. edit config file `sudo nano /etc/wsl.conf` (when needed)
      ```
      [network]
      generateResolvConf=false
      ```
2. run in powershell `wsl.exe --shutdown` as admin (shutdown wsl)
3. run in powershell `wsl.exe -l -v` as admin  (see stopped distros)
4. config Docker Desktop (settings --> Resources --> WSL integration)
5. restart wsl2
6. config minikube (see Readme) when needed
7. get local-environment-minikube
8. run ./firstBuildCluster.sh [[First Build Cluster](../install_cluster)]

---

# Nice to Have

(kubectl and helm must be installed on the host; see: [here](../install-cluster))
```text
After installing the cluster, you can interact with it using kubectl and helm in your WSL Ubuntu.
You can also interact with kubectl and helm on your host system.
Open File Explorer and click on Linux on the left, then on your Ubuntu installation.
Navigate to the /home folder and copy the .kube folder to the /Users/<Your Username> directory.
Next, go to the .kube folder and open the config file.
There are three things that need to be adjusted there:
```

1. `certificate-authority: \\wsl.localhost\Ubuntu-24.04\home\ubuntu\.minikube\ca.crt`
2. `client-certificate: \\wsl.localhost\Ubuntu-24.04\home\ubuntu\.minikube\profiles\minikube\client.crt`
3. `client-key: \\wsl.localhost\Ubuntu-24.04\home\ubuntu\.minikube\profiles\minikube\client.key`

The path `\\wsl.localhost\Ubuntu-24.04` is displayed in Windows Explorer when you click on Linux on the left and then on your Ubuntu installation.  (The path might be different.)

Result for /User/your-user/.kube/conf
```yaml
apiVersion: v1
clusters:
  - cluster:
      certificate-authority: \\wsl.localhost\Ubuntu-24.04\home\ubuntu\.minikube\ca.crt
      extensions:
        - extension:
            last-update: Thu, 30 May 2024 14:10:03 CEST
            provider: minikube.sigs.k8s.io
            version: v1.33.1
          name: cluster_info
      server: https://127.0.0.1:62920
    name: minikube
contexts:
  - context:
      cluster: minikube
      extensions:
        - extension:
            last-update: Thu, 30 May 2024 14:10:03 CEST
            provider: minikube.sigs.k8s.io
            version: v1.33.1
          name: context_info
      namespace: default
      user: minikube
    name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
  - name: minikube
    user:
      client-certificate: \\wsl.localhost\Ubuntu-24.04\home\ubuntu\.minikube\profiles\minikube\client.crt
      client-key: \\wsl.localhost\Ubuntu-24.04\home\ubuntu\.minikube\profiles\minikube\client.key
```
