---
title: "CI/CD"
linkTitle: "CI/CD"
weight: 4
description: >
  Management Guide CI/CD
---

<!-- TOC -->
* [What is CI/CD](#what-is-cicd)
* [Access Token](#access-token)
* [Gitlab CI-FILE](#gitlab-ci-file)
    * [Section in file](#section-in-file)
    * [gitlab-ci.yaml](#gitlab-ciyaml)
* [Fleet file](#fleet-file)
    * [fleet.yaml](#fleetyaml)
    * [Who to place fleet.yaml?](#who-to-place-fleetyaml)
    * [values you maybe need on the cluster](#values-you-maybe-need-on-the-cluster)
<!-- TOC -->


# What is CI/CD

```text
explain short what is cicd
```

---

# Access Token

**You need to add the current access key, to push into our container registry.
Place the Key like seen on the following picture.**

- Click on `Settings`
- Click on `CI/CD`
- Expand `Variables`
- Click on `Add Variable`
- Click ***Mask variable*** (IMPORTANT)
- add Key: `CI_MSD_CONTAINER_AUTH_TOKEN`
- add Value: ***IMPORTANT: The key is given by the DevOps Team and valid for one semester***
- Click on `Save changes`



[![token](/images/DevOps/token.png)](/images/DevOps/token.png)

---

# Gitlab CI-FILE

---

### Section in file

| Section             | Description                                                                                                 |
|---------------------|-------------------------------------------------------------------------------------------------------------|
| include             | includes our stages to build container and <br> and helm chart and publish both to our registry             |
| stages              | defines the stages of this ci-file, add more stages <br> if needed, like test, validate, build and so on    |
| variables           | defines environment variables of this script (variables set outside of this file didnt need to called here) |
| defined <br> stages | runs the defined stages in the example down the basics                                                      |

---

### gitlab-ci.yaml
```yaml
include:
  - project: "the-microservice-dungeon/devops-team/common-ci-cd"
    ref: "main"
    file:
      - "helm/package-publish.yaml"
      - "container/image-dockerfile-publish.yaml"

stages: # TODO add needed Stages like test, build and so on
  - build_container_push
  - helm

variables:
  PATH_TO_CHART: "helm-chart"
  # TODO Change Player-Name to your Player
  CHART_NAME: "player-changeme"
  # TODO Change Image name to Player-Name (end of line)
  CI_MSD_CONTAINER_REGISTRY: "registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry/changeme"



# build stage use this, to build docker image from dockerfile and push it to our container registry
container-image-publish:
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: always

helm-package-publish:
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - ${PATH_TO_CHART}/**/*
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: always


```

---

# Fleet file


### fleet.yaml
```yaml
namespace: changeme # TODO name of the namespace
helm:
  releaseName: changeme # TODO name of the name
  targetNamespace: changeme # TODO name of the namespace
  repo: "https://gitlab.com/api/v4/projects/42239222/packages/helm/stable"
  chart: "changeme" # TODO Change to Chartname

  # add values if needed (Values like in values.yaml)
  #values:

```

---

### Who to place fleet.yaml?
Inside your repo in folder `helm-charts` place the `fleet.yaml` inside.
The DevOps Team automates your Chart to the Cluster.

---

### values you maybe need on the cluster

| Value                                                                                  | Description                                |
|----------------------------------------------------------------------------------------|--------------------------------------------|
| "registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry/changeme" | where is the Docker Image                  |
| storageClassName: harvester                                                            | the storage classname of our Clusters      |
| hostname: changeme.msd.dungeon-space.de                                                | if Ingress is Used, the URL of your client |

---


---
