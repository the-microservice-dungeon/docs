---
title: "Rancher"
linkTitle: "Rancher"
weight: 2
description: >
  Management Guide Rancher
---

<!-- TOC -->
* [Management](#management)
  * [View logs](#view-logs)
  * [Delete Pod (for automate recreate)](#delete-pod-for-automate-recreate)
* [Installation](#installation)
  * [Installation Parameters on VM](#installation-parameters-on-vm)
  * [Apline Linux](#apline-linux)
  * [k3s](#k3s)
    * [Install k3s](#install-k3s)
    * [Install Cert Manager](#install-cert-manager)
    * [Install Rancher](#install-rancher)
<!-- TOC -->

# Management

## View logs
```text

```

---

## Delete Pod (for automate recreate)
```text

```

---

# Installation

## Installation Parameters on VM

```text

choosing (pictures and formatting later)

1. Lokale Iso
2. Iso abbild auswählen
3. Linux Variante: Alpine Linux
4. Speicher: 8192
5. CPU: 4
6. Benutzerdefinierter Speicher
6.1 Click Verwalten
6.2 Choose Volume (my create one)
7. Bevor install
7.1. Change name to Rancher
7.2 Change Network to msdnet
8 hit fertig an Installer starts

```

## Apline Linux

| For             | Value                    |
|-----------------|--------------------------|
| Keymap          | de                       |
| Hostname        | rancher.dungeon-space.de |
| IP Address      | 192.168.101.100          |
| Netmask         | 255.255.0.0 (/16)        |
| Create new user | NO                       |
| root password   | NOT HERE IN DOCS         |


## k3s

| For                    | Value            |
|------------------------|------------------|
| rancher admin password | NOT HERE IN DOCS |

### Install k3s

### Install Cert Manager

### Install Rancher

