---
title: "Harvester"
linkTitle: "Harvester"
weight: 3
description: >
  Management Guide Harvester
---

# Installation Parameters on VM

## Virt-Manager

```text

choosing (pictures and formatting later)

1. Lokale Iso
2. Iso abbild auswählen
3. Linux Variante: Generic Linux
4. Speicher: 120000
5. CPU: 44
6. Benutzerdefinierter Speicher
6.1 Click Verwalten
6.2 Choose Volume (my create one)
7. Bevor install
7.1. Change name to harvester
7.2 Change Network to msdnet
8 hit fertig an Installer starts

```


## Values inside of VM during installation

| For                   | Value                            |
|-----------------------|----------------------------------|
| NIC                   | only one available               |
| Hostname              | universum                        |
| IPv4 Method           | Static                           |
| IPv4 Address (Node)   | 192.168.101.201/16               |
| Gateway               | 192.168.0.1                      |
| DNS-Server            | 139.6.1.2,139.6.1.66,139.6.1.130 |
| VIP-Mode (Management) | Static                           |
| VIP IP (Management)   | 192.168.101.200                  |
| Cluster Token         | NOT HERE                         |
| Password              | NOT HERE                         |
| NTP-Servers           | Standard (its okay if fail)      |





