---
title: "Requirements"
linkTitle: "Requirements"
weight: 2
description: >
    Requirements
---

Relevant for students who are forming the DevOps team in MSD.

Have access to the Goedel Cluster via SSH.
Ask your fellow students for access.

If access is granted, and no cluster exists, the k3s cluster must be created.

---
# Installation von k3s

Execute the script "k3s_init_single_node.sh," located in the MSD/single_node folder.

Below, the script is explained.

We follow the official installation [Installation](https://ranchermanager.docs.rancher.com/getting-started/quick-start-guides/deploy-rancher-manager/helm-cli)

The first step in the guide runs the aforementioned k3s_init_single_node.sh.

PATH: <b>`./root/MSD/single_node/k3s_init_single_node.sh`</b>

<details>
<summary><b>k3s_init_single_node.sh</b></summary>

| No. | Command                                                                                        |             Description |
|:----|:-----------------------------------------------------------------------------------------------|------------------------:|
| 1   | curl -sfL https://get.k3s.io \| INSTALL_K3S_VERSION=v1.26.6+k3s1 sh -s - server --cluster-init |         install Cluster |             
| 2   | export KUBECONFIG=/etc/rancher/k3s/k3s.yaml                                                    | export config to system |
|     |                                                                                                |                         |


</details>

---

If you want to have access to the server and the cluster on your own machine, you need to follow the steps below:

LINUX: `scp root@<IP_OF_LINUX_MACHINE>:/etc/rancher/k3s/k3s.yaml ~/.kube/config`

WINDOWS: You need to use POSH-SSH since the `scp` command is not recognized.
In PowerShell, execute `Install-Module Posh-SSH`

Then, run `scp root@<IP_OF_LINUX_MACHINE>:/etc/rancher/k3s/k3s.yaml $env:USERPROFILE\.kube\config`


Now, you have downloaded the configuration file.

In that file, change the IP address in the line  ![img.png](/images/k8s/k8sServerConfigfile01.png)
to 139.6.56.53 (this is the server at the university where the cluster is accessible and may change).

Now, you can interact with the cluster from your own machine, for example, installing Helm Charts or running kubectl commands.

---
# Installation of Rancher

Here too, you should follow the official guide mentioned above.

Below is the adapted script.

PATH: <b>`./root/MSD/single_node/k3s_build_rancher_single_node.sh`</b>

<details>
<summary><b>k3s_build_rancher_single_node.sh</b></summary>

  1. #! /bin/bash
  2. export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
  3. 
  4. helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
  5. 
  6. kubectl create namespace cattle-system
  7. 
  8. kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.12.3/cert-manager.crds.yaml
  9. 
 10. helm repo add jetstack https://charts.jetstack.io
 11. 
 12. helm repo update
 13. 
 14. helm install cert-manager jetstack/cert-manager \
 15.   --namespace cert-manager \
 16.   --create-namespace \
 17.   --version v1.12.3
 18. 
 19. helm install rancher rancher-latest/rancher \
 20.   --namespace cattle-system \
 21.   --set hostname=msd.goedel.debuas.de \
 22.   --set replicas=1 \
 23.   --set bootstrapPassword=PASSWORD_FOR_RANCHER_ADMIN_MSD

Line 4: add Rancher to our helm repos

Line 6: create namespace on cluster

Line 8: add CRDs for Cert-Manager to Cluster

Line 10: add cert-manager

Line 14-17: install cert-manager

Line 19-23: install Rancher

Line 21: Hostname is the URL (webbrowser) for management

</details>


---
# Delete Cluster


PATH: <b>`./root/MSD/single_node/k3s_delete_all_single_node.sh`</b>
<details>
<summary><b>k3s_delete_all_single_node.sh </b> </summary>

  1. #! /bin/bash
  2. #
  3. /usr/local/bin/k3s-uninstall.sh

Line 3: Path to uninstall script

</details>