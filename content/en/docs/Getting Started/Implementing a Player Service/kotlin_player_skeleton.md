---
title: "Kotlin Player Skeleton"
linkTitle: "Kotlin Player Skeleton"
weight: 2
description: >
  This is the documentation for the Kotlin Player Skeleton. It is a good starting point for Kotlin developers.
  You find it
  [here](https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-kotlin).
  If you want to use it, fork this repo to your own repo, and start working.
---

{{% alert title="Hint" %}}

This following part of the documentation is embedded from the README of the
[player-skeleton-kotlin repository](https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-kotlin).
If you experience any issues on this page, just visit the repository directly.

{{% /alert %}}

{{% gitlab-md-file "https://gitlab.com/api/v4/projects/50880225/repository/files/README%2Emd?ref=main" %}}
