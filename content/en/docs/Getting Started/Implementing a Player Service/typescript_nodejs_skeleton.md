---
title: "Typescript Player Skeleton"
linkTitle: "Typescript Player Skeleton"
weight: 3
description: >
  The Typescript Player Skeleton is based on NodeJS. It is a good starting point for developers who
  are most familiar with the Typescript / Javascript way of thinking. You find it
  [here](https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-typescript-nodejs).
  If you want to use it, fork this repo to your own repo, and start working.
---

{{% alert title="Hint" %}}

This following part of the documentation is embedded from the README of the
[player-typescript-nodejs repository](https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-typescript-nodejs).
If you experience any issues on this page, just visit the repository directly.

{{% /alert %}}

{{% gitlab-md-file "https://gitlab.com/api/v4/projects/52546899/repository/files/README%2Emd?ref=main" %}}
