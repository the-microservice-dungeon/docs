---
title: "Python Player Skeleton"
linkTitle: "Python Player Skeleton"
weight: 1
description: >
  The Python Player Skeleton is written in Python 3.12. It is a good starting point for Python developers.
  You find it
  [here](https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-python).
  If you want to use it, fork this repo to your own repo, and start working.
---

{{% alert title="Hint" %}}

This following part of the documentation is embedded from the README of the
[player-skeleton-python repository](https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-python).
If you experience any issues on this page, just visit the repository directly.

{{% /alert %}}

{{% gitlab-md-file "https://gitlab.com/api/v4/projects/53419545/repository/files/README%2Emd?ref=main" %}}
