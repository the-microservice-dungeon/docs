---
title: "Java Player Skeleton"
linkTitle: "Java Player Skeleton"
weight: 1
description: >
  The Java Player Skeleton is based on Spring Boot. It is a good starting point for Java developers.
  You find it
  [here](https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-java-springboot).
  If you want to use it, fork this repo to your own repo, and start working.
---

{{% alert title="Hint" %}}

This following part of the documentation is embedded from the README of the
[player-java-springboot repository](https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-java-springboot).
If you experience any issues on this page, just visit the repository directly.

{{% /alert %}}

{{% gitlab-md-file "https://gitlab.com/api/v4/projects/48024290/repository/files/README%2Emd?ref=main" %}}
