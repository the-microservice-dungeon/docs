---
title: "Implementing a Player Service"
linkTitle: "Implementing a Player Service"
weight: 2
description: >
  This page (plus the sub-pages) describes the two basic ways to implement a player service.
---

## Two Ways to Implement a Player Service

Creating your own player service is the main purpose of the Microservice Dungeon project.
With your own player, you can control robot swarms and compete against other players. To
achieve this, you have to follow the Event-Driven Architecture style in order to react to
the events sent by the core services.

## 1. Using a Player Skeleton

There are currently four player skeletons available, which are described in detail in the
sub-pages of this page. The skeletons are available in the
[skeleton group of the Microservice Dungeon repo](https://gitlab.com/the-microservice-dungeon/player-teams/skeletons).

You basically fork the skeleton you want to use, implement your player logic and deploy it.

### How to Fork a Skeleton

You need a GitLab account to fork a skeleton. In the Gitlab project of the skeleton, click
on the **Fork** button in the upper right corner. This will create a copy of the skeleton
in your own GitLab account. You need to specify a name and a namespace for your forked
repository.
- The name should be the name of your player, for example `player-constantine`.
- The namespace is usually your own user account, but you can also create a group for your
  player and use this group as the namespace.

You should then clone the forked repository to your local machine.

### How to Take Over Updates (e.g. Bugfixes) in the Skeleton into Your Forked Repo

To take over recent changes from the upstream repository into your forked version, you can follow
these steps:

**1. Add the Upstream Repository as a Remote**

You need to add the original repository (the one you forked from) as a remote in your local Git
repository. You can do this using the following command, replacing `<upstream-url>` with the URL of the upstream
repository:

 ```bash
 git remote add upstream https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-java-springboot.git
 ```
Change URL for the other skeletons accordingly. You only need to do this step once.

**2. Fetch the Latest Changes from Upstream**

Fetch the latest changes from the upstream repository to your local repository using the following command:

 ```bash
 git fetch upstream
 ```

This command will retrieve all the changes from the upstream repository without modifying your local branches.

**3. Merge Changes**

Merge the changes from the upstream repository into your local `main` branch.

```bash
git merge upstream/main
```

**4. Resolve Conflicts (if any)**

If there are conflicts between your changes and the upstream changes, Git will prompt you to resolve them.
Open the files with conflicts, resolve the issues, and then commit the resolved changes. IDEs like IntelliJ
can help you with this process.

**5. Push the Changes to Your Fork**

After resolving conflicts (if any) and making sure everything looks good, push the changes to your forked repository:

```bash
git push origin main
```

Now, your forked repository should be up to date with the latest changes from the upstream repository.


## 2. Building a Player Service from Scratch

Only recommended if you have a good understanding of the game mechanics and architecture.
This is a good way to explore new programming languages and frameworks, though.

