---
title: "Rust Player Skeleton"
linkTitle: "Rust Player Skeleton"
weight: 4
description: >
  The Rust Player Skeleton is implemented asynchronously with the Tokio Runtime.
  It is a good starting point for Rust developers. You can find it
  [here](https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-rust).
  Fork this repo to your own repo, and start working.
---

{{% alert title="Hint" %}}

This following part of the documentation is embedded from the README of the
[player-skeleton-rust repository](https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-rust).
If you experience any issues on this page, just visit the repository directly.

{{% /alert %}}

{{% gitlab-md-file "https://gitlab.com/api/v4/projects/52546899/repository/files/README%2Emd?ref=main" %}}
