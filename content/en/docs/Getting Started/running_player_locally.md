---
title: "Running your player locally"
linkTitle: "Running your player locally"
weight: 3
description: >
  Before deploying your player service to the production environment, you can extensively run and
  test it in the local dev env. This page describes how to do this.
---


## Managing Games

When your Docker services (for all the Microservice Dungeon core services) are running, you need to manage your
games. Currently, we have the rule that only one game can be running at a time. This is to avoid confusion and
to keep the environment simple. Such a game goes through the following phases:

> created -> started -> stopped/deleted

We should look at two different modes - running a game in the local dev environment and running a game in the
production environment.

## Running a Game in the Production Environment

In the production environment, all services are running in the Kubernetes cluster. The `game` service is
manually deployed to the cluster. The `game` service is manually controlled via REST commands (either via
`curl` or Postman, or via a dashboard client). The following sequence of commands, queries and events can be
identified there.

**Legend for the sequence diagrams:**

<img src="/images/GettingStarted/Legend.jpg" width="300">
<p/>

**Sequence for deployed players in the production environment:**

[![Sequence for deployed players in the production environment](/images/GettingStarted/Sequence-in-Production-Environment.jpg)](/images/GettingStarted/Sequence-in-Production-Environment.jpg)


## Running a Game in the Local Dev Environment

The [`local-dev-environment` repository](https://gitlab.com/the-microservice-dungeon/devops-team/local-dev-environment)
contains a number of `hurl` and `sh` convenience scripts to manage games. These
scripts essentially just perform REST calls to the `game` service. You can use these scripts to create, start, stop
and delete games. You can also use them to get information about the current game.

The most convenient way manage games, however, is to use the Dev Mode in the player skeletons. It is available
in the Java skeleton (TBD - check the other skeletons).

**Sequence for player services in running locally in dev env:**

[![Sequence for player services in running locally in dev env](/images/GettingStarted/Sequence-in-Local-Dev-Mode.jpg)](/images/GettingStarted/Sequence-in-Local-Dev-Mode.jpg)

