---
title: "Setting up a local development environment"
linkTitle: "Setting up local dev env"
weight: 1
description: >
  This page describes how to set up a local development environment for the microservice dungeon project.
  With the local development environment you can develop and run your player locally. It runs the core
  services and the infrastructure (Kafka, RabbitMQ) of the Microservice Dungeon in a local docker-compose
  environment.
---

## Clone the `local_dev_env` Repository

Clone the [`DevOps-Team/local-dev-environment` Gitlab repository](https://gitlab.com/the-microservice-dungeon/devops-team/local-dev-environment)
to your local machine.

```sh
# HTTP
git clone https://gitlab.com/the-microservice-dungeon/devops-team/local-dev-environment.git

# SSH
git@gitlab.com:the-microservice-dungeon/devops-team/local-dev-environment.git
```

## Next Steps

{{% alert title="Hint" %}}

This following part of the documentation is embedded from the README of the
[local-dev-environement repository](https://gitlab.com/the-microservice-dungeon/devops-team/local-dev-environment).
If you experience any issues on this page, just visit the repository directly.

{{% /alert %}}

{{% gitlab-md-file "https://gitlab.com/api/v4/projects/34213365/repository/files/README%2Emd?ref=main" %}}
