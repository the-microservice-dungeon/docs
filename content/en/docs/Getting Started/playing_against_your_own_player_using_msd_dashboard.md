---
title: "Playing against your own player using the MSD-Dashboard"
linkTitle: "Playing against your own player using the MSD-Dashboard"
weight: 6
description: >
  When developing a player, it is challenging to assess whether a strategy, which is a crucial part of the player, is effective or ineffective. This guide shows how you can use the MSD-Dashboard to play against your own player or standard players to get a better overview of your player's performance.
---

{{% alert title="Hint" %}}

This following part of the documentation is embedded from the player-guide.md of the
[msd-dashboard repository](https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard).
If you experience any issues on this page, just visit the repository directly.

{{% /alert %}}

{{% gitlab-md-file "https://gitlab.com/api/v4/projects/53524510/repository/files/player-guide%2Emd?ref=main" %}}