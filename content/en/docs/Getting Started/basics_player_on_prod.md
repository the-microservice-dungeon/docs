---
title: "Before deploying your player to the production environment"
linkTitle: "Before deploying your player"
weight: 7
description: >
---

# How to deploy containers and Helm charts to our registry?
```text
Pushing to our registry is done through a GitLab workflow.
This involves placing the gitlab-ci.yml file in the main branch.
There, various stages are executed, from building the container image,
creating a Helm chart, and pushing it to our registry.

For our gitlab-ci.yml file, we have predefined the stage "build_container_push"
for containers and the stage "helm" for helm-charts as pipeline jobs.

Only these jobs can push to our registry.

Please follow the subsequent link to find out what settings need to be made.
```
- [MSD Gitlab CI (full)](../Infrastructure/Devops%20Guide/Management/ci_cd.md)

<details>
<summary> UNFOLD ME: How to get Access Token for Registry </summary>

---

[MSD Gitlab CI / Access Token](../Infrastructure/Devops%20Guide/Management/ci_cd.md#access-token)

**You need to add the current access key, to push into our container registry.
Place the Key like seen on the following picture.**

- Click on `Settings`
- Click on `CI/CD`
- Expand `Variables`
- Click on `Add Variable`
- Click ***Mask variable*** (IMPORTANT)
- add Key: `CI_MSD_CONTAINER_AUTH_TOKEN`
- add Value: ***IMPORTANT: The key is given by the DevOps Team and valid for one semester***
- Click on `Save changes`



[![token](/images/DevOps/token.png)](/images/DevOps/token.png)


</details>

---

# How to gain access to the cluster?
```text
If the Player Service is running locally in Minikube, it is ready to be deployed to the cluster.

For this, inform the DevOps team that you want access to the cluster.

We from the DevOps team need the following information:
1.  Name of your player
2.  Location of your GitLab repo where you are developing your player
    ( → there, under the "helm-charts" folder, the "fleet.yaml" file should be located )
3.  Optional: If your service has a web interface, we need this information
    to create an HTTP URL for you

Afterwards, the following will be set up for you:
1.  Login at rancher.dungeon-space.de
2.  Access to the cluster
3.  Access to your project and your namespace
    There, you can then manage all your resources.

What should you do?
1.  Change your password (see below)
2.  Check if you can see your player and their resources
```

---

| Picture 1 Click on the box in the top right corner and then on "Account & API Keys" | Picture 2 Now click on "Change Password" and change your password |
|-------------------------------------------------------------------------------------|-------------------------------------------------------------------|
| <img src="/images/GettingStarted/basics/change_password01.png">                     | <img src="/images/GettingStarted/basics/change_password02.png">   |

