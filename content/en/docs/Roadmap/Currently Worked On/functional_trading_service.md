---
title: "Functional Trading Service Implementation"
linkTitle: "Functional Trading Service"
layout: "newfeature/single"
# --- specific part
author: Marc Cremer
implementedDuring: Praxisprojekt Informatik Bachelor
status: finished and not merged due to incompatibility with existing services
repositories:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/trading
    branch: archived/kotlin-rewrite
    repoChanges: functional rewrite of the existing trading service
lastUpdate: 2024-08-27
description: >
  A functional rewrite of the trading service using kotlin.
---
