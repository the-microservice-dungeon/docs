---
title: "Praxisprojekt Real Time MSD"
linkTitle: "PP Real Time MSD"
layout: "newfeature/single"
type: ""
# --- specific part
author: Robin Lemmer
implementedDuring: Praxisprojekt Informatik Bachelor, WS2024/25
status: ongoing
repositories:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/game
    branch: PP-RealTime-MSD-Robin
    repoChanges:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/robot
    branch: PP-RealTime-MSD-Robin
    repoChanges:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/trading
    branch: PP-RealTime-MSD-Robin
    repoChanges:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/player-teams/standard-players/player-monte
    branch: RealTime-MSD
    repoChanges:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard
    branch: PP-RealTime-MSD-Robin
    repoChanges:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard-backend
    branch: PP-RealTime-MSD-Robin
    repoChanges:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/dashboard/gamelog
    branch: PP-RealTime-MSD-Robin
    repoChanges:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-java-springboot
    branch: PP-RealTime-MSD-Robin
    repoChanges:
lastUpdate: 2025-02-25
description: >
  This project is a continuation of the Real Time MSD
  https://the-microservice-dungeon.gitlab.io/docs/docs/roadmap/currently-worked-on/real_time_msd/,
  that already has a developed basis consisting of loose services. The core-services are extended by
  some functionalities. Player Monte and the java skeleton are adapted to work with the RT-MSD.
  The dashboard has a functional controlpanel and map. A redis server which manages the robot-locking
  has been added to the infrastructure. End goal is a functional local-dev-environment for the Real Time MSD.
---

## Table of Contents
1. [Architecture](#architecture)
2. [Events](#events)
   1. [Changed Event](#changed-event)
   2. [New Event](#new-event)
3. [Action Duration](#action-duration)
4. [Robot Locking](#robot-locking)
5. [Robot Upgrades](#robot-upgrades)
   1. [New Upgrades](#new-upgrades)
   2. [Upgrade Changes](#upgrade-changes)
   3. [Upgrade Values](#upgrade-values)
6. [MSD Dashboard](#msd-dashboard)
7. [TODO](#todo)



### Architecture


![The new Architecture](/images/realtimeMSD/new_architecture.png)



### Events


#### Changed Event

**RobotsRevealedEvent**: Reveals all robots on the planet to the player.
* contains the robotRevealed data of all robots on the planet
* is sent when a robot spawns or moves
* is only sent to the player of the robot who triggered the event


#### New Event

**ActivityDetectedEvent**: Informs players with robots on a planet when activity was executed by another robot.
* contains the open data (robotRevealed) of a single robot (for more information check out the [Asyncapi](../realtime-msd/robot/asyncapi/))
* is sent after following robot actions: `move`, `fight`, `regenerate`, `upgrade`, `restoration`
* is sent to every player that has a robot standing on the planet where the activity happened. For `move` that means
players from both the source and target planet.



### Action Duration

In order to keep a balanced game experience we introduce action times. Those times determine how long it takes until the action is completed.

| Action       | Time  |
|--------------|-------|
| Fighting     | 3 Sek |
| Mining       | 3 Sek |
| Movement     | 3 Sek |
| Regenerate   | 3 Sek |
| Upgrade      | 3 Sek |

Timings are currently only temporary and are subject to change in order to improve the game balance.



### Robot Locking

A robot can only execute 1 command at a time. This is insured by the robot command locker. It uses a local Redis server
with a distributed lock pattern to save (lock) the IDs of robots that are currently performing an action. When the action
duration is over, the robotID is released (unlock) again. In case of errors the robotIDs are automatically released after
a certain time to avoid deadlocks.



### Robot Upgrades


#### New Upgrades

- Movement Speed: reduces travel time between planets
- Regeneration Speed: reduces regeneration time
- Mining Efficiency: replaces the old mining speed upgrade
- Attack Speed: reduces attack time


#### Upgrade Changes

- Mining Speed: reduces mining time


#### Upgrade Values

| Upgrade              | Effect                      | Level 0 | Level 1 | Level 2 | Level 3 | Level 4 | Level 5 |
|----------------------|-----------------------------|---------|---------|---------|---------|---------|---------|
| Attack Speed         | Percentage of action time   | 100 %   | 80 %    | 65 %    | 50 %    | 35 %    | 20 %    |
| Mining Speed         | Percentage of action time   | 100 %   | 80 %    | 65 %    | 50 %    | 35 %    | 20 %    |
| Movement Speed       | Percentage of action time   | 100 %   | 80 %    | 65 %    | 50 %    | 35 %    | 20 %    |
| Regeneration Speed   | Percentage of action time   | 100 %   | 80 %    | 65 %    | 50 %    | 35 %    | 20 %    |
| Mining Efficiency    | Mining Amount               | 2       | 5       | 10      | 15      | 20      | 40      |

**Note**: While the effect improvement per level of the speed upgrades is on paper not as high as the existing upgrades,
it allows for more flexibility and better reaction time.



### MSD Dashboard

The controlpanel and the map with the sidebar are fully functional. The map is updated every 0.5 seconds.
The statistics are not working with the Real Time MSD.


### MONTE and Skeletons

Player MONTE and the Java Skeleton have been modified to be compatible with the Real Time MSD.



### TODO

The following things still need implementation or some changes:

- PriceChangedEvent (needs trading economics)
- Change other skeletons to work in the realtime msd
- Tests are outdated
- Add the new capabilities in msd-dashboard-backend
- Websockets in dashboard
- Kubernetes


---


## How to use the Local-Dev-Environment of the Real Time MSD

### Clone the `local_dev_env` Repository

Clone the [`DevOps-Team/local-dev-environment` Gitlab repository](https://gitlab.com/the-microservice-dungeon/devops-team/development-envs/local-dev-environment/-/tree/PP-RealTime-MSD-Robin?ref_type=heads)
to your local machine and switch to branch `PP-RealTime-MSD-Robin`.

```sh
# HTTP
git clone https://gitlab.com/the-microservice-dungeon/devops-team/local-dev-environment.git

# SSH
git@gitlab.com:the-microservice-dungeon/devops-team/local-dev-environment.git
```
```sh
# Switch Branch (if branch does not exist locally yet)
git checkout -b PP-RealTime-MSD-Robin origin/PP-RealTime-MSD-Robin
```

### Next Steps

Follow the README of the
[local-dev-environement repository (Branch: PP-RealTime-MSD-Robin)](https://gitlab.com/the-microservice-dungeon/devops-team/development-envs/local-dev-environment/-/tree/PP-RealTime-MSD-Robin?ref_type=heads).


---


## API Reference

#### [Game Service](../realtime-msd/game/)

#### [Robot Service](../realtime-msd/robot/)

#### [Trading Service](../realtime-msd/trading/)

###### The [API Reference](docs/roadmap/currently-worked-on/realtime-msd) lists more services, which have not been altered for the Real Time MSD.


