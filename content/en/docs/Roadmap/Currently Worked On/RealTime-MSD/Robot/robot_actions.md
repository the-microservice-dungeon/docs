---
title: "Robot Actions"
linkTitle: "Robot Actions"
weight: 2
description: >
  Robot Actions
---

# Robot Actions

## Commands

:::info

Not all action related events are presented here with an actual example, because they work in the same manner.
For the missing Events please refer to the [ASYNC API](/docs/roadmap/currently-worked-on/realtime-msd/robot/asyncapi/)

:::


### Valid command types for robot

    move
    fight
    mine
    regenerate


## Actions

Most of the actions will require energy. If there is not enough energy left in the robot an action will fail.
Every action has an action Duration, after which the command finishes processing. During that time, the same robot can't
send another command.

### Movement action

* robot service: receives and processes the command, issues request to map, checks if two planets are neighbours, processes the results and throws event according to the result
* map service: provides neighbours of a planet

A successful result of the move must include all planet data of the new position. This info must be obfuscated so that not every player can just read the most recent planet data of all visited planets. Therefore, the planet info must be obfuscated via the command uuid.
After a successful movement two events are thrown. The first one which indicates the success of the movement. It contains the remaining energy of the robot, the planet data of the target planet and the uuids of all robots located there. The second event is mapped to the command uuid and provides all neighbours for the target planet after a successful move.
If a player tries to move a robot which is on an unreachable planet, the robot service will just throw an event to report the failure for that specific robot.
<!--
### Item usage action

* Game service: issues the command which was received by the player
* Robot service: processes the command, and throws events according to the result
* Special case movement item: robot determines a random planet for which the map service: provides a list of all planets

(https://github.com/The-Microservice-Dungeon/robot/tree/main/src/main/kotlin/com/msd/item/domain)

#### Fighting item

The received item type determines if a robot uuid or a planet uuid is expected. E.g., robot uuid for the targeted rocket and planet uuid for the nuke.

#### Repair item

The received item type determines if a robot uuid or a planet uuid is expected. E.g., robot uuid for a single repair and planet uuid for the repair swarm.

#### Movement item (wormhole)

The robot service requests a list of all planets from the map service and chooses a random one. The location of the robot is changes accordingly and a success event is thrown. Additionally, the two events after a successful movement (refer to Robot movement sequence) are also thrown.
-->
### Fighting action

* Robot service: receives and processes the command and throws event according to the result.


### Mining action

* Robot service: receives and processes the command, issues requests to map, processes the results and throws event according to the result
* Map service: handles the amount of available resources

To determine if the requests is valid and the corresponding robot can mine the resource on its location, the robot service first requests the type of the resource from the map service.
The robot service then sends a mining request to the map service. Map then returns the amount which can be mined (requested value or below).

### Regeneration action

* Robot service: receives and processes the command and throws event according to the result

Be careful not to mix up with the regenerating, you can buy with a command to the trading service. This action does not require energy to be used.
