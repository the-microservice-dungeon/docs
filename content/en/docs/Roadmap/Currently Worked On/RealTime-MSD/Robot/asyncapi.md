---
title: "AsyncAPI (Real Time MSD)"
linkTitle: "AsyncAPI (Real Time MSD)"
weight: 10
type: asyncapi
description: >
  AsyncAPI for the Real Time MSD
---

{{< asyncapi src="/docs/roadmap/currently-worked-on/realtime-msd/robot/asyncapi.yaml" >}}
