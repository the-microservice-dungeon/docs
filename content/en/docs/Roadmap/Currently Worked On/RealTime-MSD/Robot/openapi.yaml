openapi: 3.0.1
info:
  title: Robot Service API
  description: This is REST documentation for the Robot Service
  version: v5
components:
  schemas:
    robot:
      type: object
      properties:
        id:
          type: string
          format: uuid
        player:
          type: string
          format: uuid
        planet:
          type: string
          format: uuid
        alive:
          type: boolean
          example: true
        maxHealth:
          type: integer
          example: 100
        maxEnergy:
          type: integer
          example: 60
        energyRegen:
          type: integer
          example: 8
        attackDamage:
          type: integer
          example: 5
        miningEfficiency:
          type: integer
          example: 10
        miningSpeed:
          type: number
          format: double
          example: 1.0
        movementSpeed:
          type: number
          format: double
          example: 1.0
        attackSpeed:
          type: number
          format: double
          example: 1.0
        energyRegenSpeed:
          type: number
          format: double
          example: 1.0
        health:
          type: integer
          example: 75
        energy:
          type: integer
          example: 43
        healthLevel:
          type: integer
          minimum: 0
          maximum: 5
        damageLevel:
          type: integer
          minimum: 0
          maximum: 5
        miningSpeedLevel:
          type: integer
          minimum: 0
          maximum: 5
        miningLevel:
          type: integer
          minimum: 0
          maximum: 5
        miningEfficiencyLevel:
          type: integer
          minimum: 0
          maximum: 5
        energyLevel:
          type: integer
          minimum: 0
          maximum: 5
        energyRegenLevel:
          type: integer
          minimum: 0
          maximum: 5
        energyRegenSpeedLevel:
          type: integer
          minimum: 0
          maximum: 5
        movementSpeedLevel:
          type: integer
          minimum: 0
          maximum: 5
        attackSpeedLevel:
          type: integer
          minimum: 0
          maximum: 5
        storageLevel:
          type: integer
          minimum: 0
          maximum: 5
        inventory:
          type: object
          properties:
            maxStorage:
              type: integer
              example: 20
            usedStorage:
              type: integer
              example: 5
            coal:
              type: integer
              example: 3
            iron:
              type: integer
              example: 2
            gem:
              type: integer
              example: 0
            gold:
              type: integer
              example: 0
            platin:
              type: integer
              example: 0
    gameplay-variables:
      type: object
      properties:
        storage:
          type: object
          properties:
            lvl0:
              type: integer
              example: 20
            lvl1:
              type: integer
              example: 50
            lvl2:
              type: integer
              example: 100
            lvl3:
              type: integer
              example: 200
            lvl4:
              type: integer
              example: 400
            lvl5:
              type: integer
              example: 1000
        hp:
          type: object
          properties:
            lvl0:
              type: integer
              example: 10
            lvl1:
              type: integer
              example: 25
            lvl2:
              type: integer
              example: 50
            lvl3:
              type: integer
              example: 100
            lvl4:
              type: integer
              example: 200
            lvl5:
              type: integer
              example: 500
        damage:
          type: object
          properties:
            lvl0:
              type: integer
              example: 1
            lvl1:
              type: integer
              example: 2
            lvl2:
              type: integer
              example: 5
            lvl3:
              type: integer
              example: 10
            lvl4:
              type: integer
              example: 20
            lvl5:
              type: integer
              example: 50
        miningSpeed:
          type: object
          properties:
            lvl0:
              type: integer
              example: 2
            lvl1:
              type: integer
              example: 5
            lvl2:
              type: integer
              example: 10
            lvl3:
              type: integer
              example: 15
            lvl4:
              type: integer
              example: 20
            lvl5:
              type: integer
              example: 40
        energyCapacity:
          type: object
          properties:
            lvl0:
              type: integer
              example: 20
            lvl1:
              type: integer
              example: 30
            lvl2:
              type: integer
              example: 40
            lvl3:
              type: integer
              example: 60
            lvl4:
              type: integer
              example: 100
            lvl5:
              type: integer
              example: 200
        energyRegeneration:
          type: object
          properties:
            lvl0:
              type: integer
              example: 4
            lvl1:
              type: integer
              example: 6
            lvl2:
              type: integer
              example: 8
            lvl3:
              type: integer
              example: 10
            lvl4:
              type: integer
              example: 15
            lvl5:
              type: integer
              example: 20
        energyCostCalculation:
          type: object
          properties:
            miningMultiplier:
              type: integer
              example: 1
            miningWeight:
              type: integer
              example: 1
            movementMultiplier:
              type: integer
              example: 1
            attackingMultiplier:
              type: integer
              example: 1
            attackingWeight:
              type: integer
              example: 1
    commands:
      type: object
      required:
        - robotId
        - command
        - playerId
        - transactionId
      properties:
        robotId:
          type: string
          format: uuid
        transactionId:
          type: string
          format: uuid
        playerId:
          type: string
          format: uuid
        command:
          type: string
          enum: [ "mining", "movement", "battle", "regenerate" ]
        planetId:
          type: string
          format: uuid
        targetId:
          type: string
          format: uuid
paths:
  "/robots":
    post:
      summary: Spawns a robot
      description: If you want spawn more than one robot at a time, you can use the attribute "quantity"
      tags:
        - robot
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                type: array
                items:
                  "$ref": "#/components/schemas/robot"
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                type: string
                example: Request could not be accepted
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                transactionId:
                  type: string
                  format: uuid
                  description: Amount of items must be equal to quantity
                player:
                  type: string
                  format: uuid
                quantity:
                  type: integer
                  minimum: 1
              required:
                - transactionId
                - player
                - planet
    get:
      summary: Reveice data about all robots belonging to a specified player
      tags:
        - robot
      responses: #TODO POST verbieten in bestimmten Faellen
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  "$ref": "#/components/schemas/robot"
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                type: string
                example: Request could not be accepted
        '404': #player existiert nicht
          description: Not Found
          content:
            application/json:
              schema:
                type: string
                example: "Player not found"
      parameters:
        - name: player-id
          in: query
          schema:
            type: string
            format: uuid


  "/robots/{robot-uuid}":
    get:
      summary: Receive robot data
      tags:
        - robot
      parameters:
        - name: robot-uuid
          in: path
          required: true
          schema:
            type: string
      responses:
        '200': #response für ungültige ID und kein Zugriff erlaubt
          description: Return data of requested robot
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/robot"
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                type: string
                example: Request could not be accepted
  "/robots/{robot-uuid}/upgrades":
    post:
      summary: Upgrade a robot
      tags:
        - trading
      parameters:
        - name: robot-uuid
          in: path
          required: true
          schema:
            type: string
      responses:
        '200': #upgrade ist erlaubt
          description: OK
          content:
            application/json:
              schema:
                type: string
                example: "Energy capacity of robot 3fa85f64-5717-4562-b3fc-2c963f66afa6 has been upgraded to lvl3."
        '409': #upgrade nicht erlaubt
          description: Upgrade not possible
          content:
            application/json:
              schema:
                type: string
                example: "Upgrade of robot 3fa85f64-5717-4562-b3fc-2c963f66afa6 rejected. Current lvl of Energy capacity is 5."
        '400': #sonstiger Fehler
          description: Bad Request
          content:
            application/json:
              schema:
                type: string
                example: "Request could not be accepted"
        '404': #robot existiert nicht
          description: Not Found
          content:
            application/json:
              schema:
                type: string
                example: "Robot not found"
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                transactionId:
                  type: string
                  format: uuid
                upgradeType:
                  type: string
                  enum: [ STORAGE, HEALTH, DAMAGE, MINING_SPEED, MINING, MAX_ENERGY, ENERGY_REGEN, MOVEMENT_SPEED,
                          ATTACK_SPEED, MINING_EFFICIENCY, ENERGY_REGEN_SPEED ]
                  description: When using this endpoint, keep in mind that upgradeType is case sensitive.
                targetLevel:
                  type: integer
                  minimum: 1
                  maximum: 5
              required:
                - transactionId
                - upgradeType
                - targetLevel

  "/robots/{robot-uuid}/instant-restore":
    post:
      summary: Fully restore energy or health of a robot
      tags:
        - trading
      parameters:
        - name: robot-uuid
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: string
                example: "robot 3fa85f64-5717-4562-b3fc-2c963f66afa6 has been fully healed"
        '400': #Falscher item-type
          description: Bad Request
          content:
            application/json:
              schema:
                type: string
                example: "Request could not be accepted"
        '404':
          description: Not Found
          content:
            application/json:
              schema:
                type: string
                example: "Robot not found"
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                transactionId:
                  type: string
                  format: uuid
                restorationType:
                  type: string
                  enum: [ HEALTH, ENERGY ]
                  description: When using this endpoint, keep in mind that restorationType is case sensitive.
              required:
                - transactionId
                - restorationType
  "/robots/{robot-uuid}/inventory/clear-resources":
    post:
      summary: Sell all resources
      description: Using this endpoint clears all ressources and returns the state of the robot's inventory before the clear.
      tags:
        - trading
      parameters:
        - name: robot-uuid
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  coal:
                    type: integer
                    example: 3
                  iron:
                    type: integer
                    example: 2
                  gem:
                    type: integer
                    example: 0
                  gold:
                    type: integer
                    example: 0
                  platin:
                    type: integer
                    example: 0
        '400': #sonstiger Fehler
          description: Bad Request
          content:
            application/json:
              schema:
                type: string
                example: "Request could not be accepted"
        '404': #robot existiert nicht
          description: Not Found
          content:
            application/json:
              schema:
                type: string
                example: "Robot not found"
  "/commands":
    post:
      summary: Post a command to be executed
      description: "This endpoint allows the player to send a command to be executed."
      tags:
        - commands
      responses:
        '202':
          description: Accepted
          content:
            application/json:
              schema:
                type: string
                example: Command accepted
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                type: string
                example: Request could not be accepted
        '409':
          description: Conflict
          content:
            application/json:
              schema:
                type: string
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/commands"
servers:
  - url: http://{defaultHost}
    variables:
      defaultHost:
        default: localhost:8082
