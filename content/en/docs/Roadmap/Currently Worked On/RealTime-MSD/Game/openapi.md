---
title: "OpenAPI (Real Time MSD)"
linkTitle: "OpenAPI (Real Time MSD)"
weight: 9
type: swagger
description: >
  OpenAPI for the Real Time MSD
---

{{< swaggerui src="/docs/roadmap/currently-worked-on/realtime-msd/game/openapi.yaml" >}}
