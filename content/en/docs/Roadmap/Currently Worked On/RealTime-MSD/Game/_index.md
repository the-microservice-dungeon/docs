---
title: "Game Service"
linkTitle: "Game Service"
weight: 1
description: >
  Game Service
---

The game service is responsible for creating, starting and ending games, as well as creating and registering players.

The players receive an id and a queue, which they can use to send commands and receive the outcome.

## Events

### Before a game

Before the start of a game there are following game service related events:

* Game Created

* Player "name" joined the game. (optional, if no player joins then this event is not produced)

* Game started

* *Excursus:* The game world is created through a REST call to the [map service](/docs/roadmap/currently-worked-on/realtime-msd/map/). The game world is automatically sized to the created game maximal player.




### Game ending

The final event is:

* Game ended (with this event a game ends. No commands can be issued anymore. GameLog service should provide a scoreboard and trophies. When a new game starts, you have to join it again to play.)

---

## Repository Link Game

[Repository Link Game](https://gitlab.com/the-microservice-dungeon/core-services/game/-/tree/PP-RealTime-MSD-Robin?ref_type=heads)
