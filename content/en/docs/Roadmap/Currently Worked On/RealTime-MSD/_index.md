---
title: "API Reference (RealTime-MSD)"
linkTitle: "API Reference (RealTime-MSD)"
cascade:
  - type: ""
description: >
  This page lists the API documentation for the Real Time Microservice Dungeon, mainly those that might differ from
  the turn-based dungeon. This in an altered version of the already existing `API Reference`, by Robin Lemmer.
---
