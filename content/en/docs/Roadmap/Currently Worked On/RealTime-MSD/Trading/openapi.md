---
title: "OpenAPI"
linkTitle: "OpenAPI"
weight: 9
type: swagger
description: >
  OpenAPI
---

{{< swaggerui src="https://gl.githack.com/the-microservice-dungeon/core-services/trading/-/raw/main/swagger/v1/swagger.yaml" >}}
