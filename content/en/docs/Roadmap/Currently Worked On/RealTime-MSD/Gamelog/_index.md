---
title: "Gamelog Service"
linkTitle: "Gamelog Service"
weight: 5
description: >
  Gamelog Service
---

# GameLog Service Technical View

{{% alert title="Warning" color="warning" %}}
Gamelog is currently in a state of being rewritten. The documentation here is outdated.
Please refer to [GameLog Repository](https://gitlab.com/the-microservice-dungeon/core-services/gamelog)
{{% /alert %}}

There are multiple scoreboards:

* The global scoreboard

* Multiple scoreboards for the different event categories:
        Fighting
        Mining
        Traveling
        Trading

The weighting for calculating the scores won't be changeable during the game.

## Repository Link GameLog

[Repository Link GameLog](https://gitlab.com/the-microservice-dungeon/core-services/gamelog)
