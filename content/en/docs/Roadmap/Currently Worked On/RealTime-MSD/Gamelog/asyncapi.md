---
title: "AsyncAPI"
linkTitle: "AsyncAPI"
weight: 10
type: asyncapi
description: >
  AsyncAPI
---

{{< asyncapi src="https://gl.githack.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/docs/event-spec.yaml" >}}
