---
title: "DevOps-Team Contribution of Omar"
linkTitle: "DevOps-Team Contribution of Omar"
layout: "newfeature/single"
author: Omar Fourati
implementedDuring: WASP II "Microservices und Event-getriebene Architektur" (MEA - Grundlagenteil), SS2024
status: ongoing
repositories:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/devops-team/conventions
    branch: 1-omars-conventions
    repoChanges: Learn what helm charts are and develop a minimal Helm chart for MariaDB Helmchart from Bitnami. Changes have already been edited in all repositories to use Bitnami.
  - repoUrl: https://gitlab.com/the-microservice-dungeon/devops-team/local-environment-minikube
    branch: main
    repoChanges: Update to the new structure of Minikube and Kubernetes. Add the new Helm charts for the core services.

lastUpdate: 2024-08-10
description: >
  During the SS2024 session of the WASP II program focusing on "Microservices and Event-driven Architecture," I have actively contributed to the DevOps team by enhancing our project's infrastructure and deployment strategies. My contributions include mastering Helm charts and applying this knowledge to integrate the MariaDB Helm chart from Bitnami into our service deployments. Furthermore, I have restructured our local Minikube setup to align with the latest Kubernetes standards, which involved updating the Helm charts for core services. These efforts have streamlined our development processes and improved the deployment efficiency of our microservices architecture.
---

## Contributions Overview

### Helm Charts for MariaDB
- **Objective**: Simplify database deployments across multiple environments.
- **Outcome**: Developed a minimal Helm chart for MariaDB, leveraging the Bitnami Helm chart as a foundation. This integration standardizes our database setups and reduces deployment complexities.

### Minikube and Kubernetes Structure Updates
- **Objective**: Update and optimize the local development environment to support the latest Kubernetes features.
- **Outcome**: Revamped the Minikube setup and integrated new Helm charts for core services. This update enhances local testing capabilities and better simulates our production environment.

These contributions are pivotal for maintaining our project's competitiveness and agility in adopting new technologies and architectural paradigms.
