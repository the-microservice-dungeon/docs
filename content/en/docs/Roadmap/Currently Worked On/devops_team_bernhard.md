---
title: "DevOps-Team Contribution of Bernhard"
linkTitle: "DevOps-Team Contribution of Bernhard"
layout: "newfeature/single"
# --- specific part
author: Bernhard Markus Sprenger
implementedDuring: WASP II "Microservices und Event-getriebene Architektur" (MEA - Grundlagenteil), SS2024
status: ongoing
repositories:
- repoUrl: https://gitlab.com/the-microservice-dungeon/devops-team/conventions
  branch: bernhard_conventions
  repoChanges: learn what helm charts are and do a minimal helm chart for MariaDB Helmchart from Bitnami. Has already been editet in all Repos to use Bitnami.
- repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/gamelog
  branch: main
  repoChanges: Update helm chart to the new standard
- repoUrl: https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-kotlin
  branch: main
  repoChanges: Update helm chart to the new standard
- repoUrl: https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-python
  branch: main
  repoChanges: Update helm chart to the new standard
- repoUrl: https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-rust/
  branch: main
  repoChanges: Update helm chart to the new standard
- repoUrl: https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-typescript-nodejs/
  branch: main
  repoChanges: Update helm chart to the new standard
- repoUrl: https://gitlab.com/the-microservice-dungeon/devops-team/deployments/values/
  branch: bSp/db-values
  repoChanges: Bitnami Helmcharts for the DB with Logins configured to be deploy ready and make it the central db and not each service having its own. Postgres technicly done but the last script deployments failed.

lastUpdate: 2024-08-10
description: >

  Results of my partipication of the DevOps Team of SS2024
  
---

 Contribute to the making of a new standard of how the costume helm charts are structured and then applied these to the core service gamelog and the player skeletons kotlin, python, rust, type-script. Changes have already been made and merged.

 The configuration of the Bitnami MariaDB chart so it can be made into a central DB reducing the resources need for deployment in minikube or Kubernetes. The Bitnami has a sh script in its value file that setups all needed namespaces, databases, users for each core service. The value file is completely deployment ready and only waits for the changes in the core services helm charts to link to it and have it in the deployment systemt for kubernetes and minikube.

The configuration of the Bitnami PostgresSQL DB to be a central DB like MariaDB with the same goals of reduces need deploment resources. It also incoperates a sh script in its value file to setup the namespaces, databases, users for the services that depened on it. After the requirements of how to setup the user, databases were changed from sql file to sh script for potential more posiibities, the Script would no longer work and still needs to be fixed. The individual commands are verified that they work.

The surrealDB configuration helm chart is a one to one extraction from dashboard as the only service that uses it. It was attempted to make it have a setup script like the other two DB but the complexity of the helm chart configuration has made it not worth it as only one service uses it.

During the Developement a lot about Kubernetes and Helm Charts were learned and also how not User friendly Helm Charts can be if one make one from Scratch or try to Change one which one did not make themselves. This further was made difficult by the bad and sometimes confusing documentation of it. The existion of Bitnami to have a chart standard with common variables to change and with all possible need changes configurable from the value file and good standard values make Helm Chart a lot easier.
