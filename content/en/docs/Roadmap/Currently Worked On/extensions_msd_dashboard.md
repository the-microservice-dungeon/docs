---
title: "Extensions for the MSD-Dashboard"
linkTitle: "Extensions for the MSD-Dashboard"
layout: "newfeature/single"
# --- specific part
author: Florian Lemmer
implementedDuring: Praxisprojekt Informatik Bachelor
status: finished, but not yet reviewed and merged
repositories:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard
    branch: new-features-florian
    repoChanges: added all new features mentioned above, some changes are already merged with the main-branch
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard-docker-api
    branch: 
    repoChanges: new repository containing an api for the dashboard to communicate with docker
  - repoUrl: https://gitlab.com/the-microservice-dungeon/docs
    branch: MSD-Dashboard
    repoChanges: documentation on Dashboard,Dashboard-Docker-Api and player-guide for playing against own player
lastUpdate: 2024-09-02
description: >
  The Dashboard is a tool that offers real-time monitoring of an existing game through a live map displaying robots, resources, and detailed information about participating players and planets.
  The Dashboard will be extended by the following features:
    - Party Creation: Player can add standard players and their own player to the game
    - Match Statistics: Charts display information about the current score of the game
    - Player Maps: Option to switch to player specific maps showing the perspective of a single player
    - Play-Again-Button: Button for playing again with same settings including game configurations and added players
---

{{% alert title="Hint" %}}

This following part of the documentation is embedded from the README of the
[msd-dashboard repository](https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard).
If you experience any issues on this page, just visit the repository directly.

{{% /alert %}}

{{% gitlab-md-file "https://gitlab.com/api/v4/projects/53524510/repository/files/README%2Emd?ref=main" %}}