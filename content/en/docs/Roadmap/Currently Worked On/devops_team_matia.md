---
title: "DevOps-Team Contribution of Matia"
linkTitle: "DevOps-Team Contribution of Matia"
layout: "newfeature/single"
# --- specific part
author: Matia Leon Schumacher
implementedDuring: WASP II "Microservices und Event-getriebene Architektur" (MEA - Grundlagenteil), SS2024, as WPF replacement
status: ongoing
repositories:
- repoUrl: https://gitlab.com/the-microservice-dungeon/devops-team/conventions
  branch: matia_conventions
  repoChanges: helm chart conventions (already finished, reviewed & "merged" into main during a dev ops meeting, actually merging the branch into main is not planed)
- repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/map
  branch: main
  repoChanges: helm chart
- repoUrl: https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-java-springboot
  branch: main
  repoChanges: helm chart
- repoUrl: https://gitlab.com/the-microservice-dungeon/devops-team/common-ci-cd
  branch: main
  repoChanges: helm lint job
- repoUrl: https://gitlab.com/the-microservice-dungeon/devops-team/common-ci-cd
  branch: main
  repoChanges: local-dev-environment stage
- repoUrl: https://gitlab.com/the-microservice-dungeon/devops-team/common-ci-cd
  branch: main
  repoChanges: local-mock-environment stage
- repoUrl: https://gitlab.com/the-microservice-dungeon/devops-team/common-ci-cd
  branch: refactor_and_add_rules
  repoChanges: rules of build stage
- repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/game
  branch: integration_test_stage
  repoChanges: removing testcontainer framework & adding integration tests to pipeline
- repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/map
  branch: integration_test_stage
  repoChanges: removing testcontainer framework & adding integration tests to pipeline
- repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/robot
  branch: integration_test_stage
  repoChanges: removing testcontainer framework & adding integration tests to pipeline
lastUpdate: 2024-08-10
description: >
  Participation at the dev ops team of SS2024. The issues I worked on/contributed to, are:

  Contribution to the creation of our helm conventions and the subsequent modification of the existing helm charts. My responsibility were the helm charts of the map core service and the java skeleton (finished, reviewed & merged).

  Integration of a common-ci-cd stage regarding a syntactic helm chart validation via helm lint (finished, reviewed & merged).

  Integration of a common-ci-cd stage regarding the usage of the local-dev-environment for testing purposes in the pipeline (finished, reviewed & merged).

  Integration of a common-ci-cd stage regarding the usage of the local-mock-environment for testing purposes in the pipeline (finished, reviewed & merged).

  A presentation of the pipeline (CI & CD) of the Microservice Dungeon (finished/presented).

  Refactorisation of the rules aspect of the common-ci-cd build stage softly limiting build process executions to branch merges onto main only (finished, not yet reviewed).

  Refactorisation of the integration tests of the game core service to remove the test container framework (finished, not yet reviewed).

  Integration of the tests (unit & integration) into the pipeline of the game core service (finished, not yet reviewed).

  Refactorisation of the integration tests of the map core service to remove the test container framework (finished, not yet reviewed).

  Integration of the tests (unit & integration) into the pipeline of the map core service (finished, not yet reviewed).

  Refactorisation of the integration tests of the robot core service to remove the test container framework (finished, not yet reviewed).

  Integration of the tests (unit & integration) into the pipeline of the robot core service (finished, not yet reviewed).
---

(tbd)
