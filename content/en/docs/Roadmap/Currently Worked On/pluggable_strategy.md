---
title: "Pluggable Strategy and Typical Strategy Patterns"
linkTitle: "Pluggable Strategy and Typical Strategy Patterns"
layout: "newfeature/single"
# --- specific part
author: Malte Boettcher
implementedDuring: WASP II "Microservices und Event-getriebene Architektur (MEA), SS2024, as topic (P2)
status: ongoing
repositories:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/docs/-/tree/Pluggable_strategy/content/en/docs/Getting%20Started?ref_type=heads
    branch: Pluggable_strategy
    repoChanges: Added file
#  Unable to provide in any other way due to being a private repository
#  - repoUrl: https://gitlab.com/the-microservice-dungeon/player-teams/standard-players/player-monte
#    branch: Pluggable_strategy
#    repoChanges: None so far
lastUpdate: 2024-09-04
description: >
  What are general strategies for controlling robots? And how do you make the strategy interchangeable,
  and separate it from the other domain logic? This feature will research typical strategies and develop
  an architecture pattern for the dungeon players, to make strategies interchangeable.
  There will be a demo implementation on one or more standard players.
---

### What happened so far?
- Researched how best to develop a pluggable strategy in java
- Checked out how strategies are realize in player M.O.N.T.E.

### What will happen next?
- Implement feature to alter the strategy of player M.O.N.T.E. based on given parameters
- Add guide of how strategies could be implemented for future players
