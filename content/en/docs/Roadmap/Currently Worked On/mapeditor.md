---
title: "Map WYSIWYG Editor"
linkTitle: "Map WYSIWYG Editor"
layout: "newfeature/single"
# --- specific part
author: Sarah Poloczek
implementedDuring: Praxisprojekt Informatik Bachelor
status: finished, but not yet reviewed and merged
repositories:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/map-editor
    branch:
    repoChanges: new repository containing the Map editor client
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/map
    branch: map-editor
    repoChanges: added REST interface to control the map creation and editing
  - repoUrl: https://gitlab.com/the-microservice-dungeon/docs
    branch: MapEditor
    repoChanges: documentation on how to use the editor client
lastUpdate: 2024-07-30
description: >
  Map WYSIWYG Editor is a tool that allows users to create and edit maps in a graphical way.
---

{{% alert title="Hint" %}}

This following part of the documentation is embedded from the README of the
[map-editor repository](https://gitlab.com/the-microservice-dungeon/core-services/map-editor).
If you experience any issues on this page, just visit the repository directly.

{{% /alert %}}

{{% gitlab-md-file "https://gitlab.com/api/v4/projects/56020910/repository/files/README%2Emd?ref=main" %}}
