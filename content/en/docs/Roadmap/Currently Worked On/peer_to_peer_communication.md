---
title: "Peer to Peer Communication"
linkTitle: "Peer to Peer Communication"
layout: "newfeature/single"
# --- specific part
author: Philipp Teichert
implementedDuring: Praxisprojekt Informatik Bachelor
status: ongoing
repositories:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/devops-team/development-envs/local-dev-environment
    branch: fix/map-not-showing-on-dashboard
    repoChanges: branch for fixing an issue that prevents the map to show on the dashboard
lastUpdate: 2024-10-29
description: >
  toDo
---

### What happened so far?
An opinion poll was formulated with google forms.
While learning the msd and testing on the local dev environment, I fixed the map not showing bug on the local_dev_environment.

### Opinion poll results
toDo
