---
title: "Improved Player Dev Env"
linkTitle: "Improved Player Dev Env"
layout: "newfeature/single"
# --- specific part
author: Deniz Kaan Büyüktas, Moritz Kohler
implementedDuring: WASP II "Microservices und Event-getriebene Architektur (MEA), SS2024, as topic (P3)
status: ongoing
repositories:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/docs/-/tree/Player-Dev-Env/content/en/docs/Getting%20Started/Improved%20Player-Dev-Env?ref_type=heads
    branch: Player-Dev-Env
    repoChanges: New Folder:"Improved Player-Dev-Env" with files of the project, new images and videos
lastUpdate: 2024-09-04
description: >
  Player development is still very much a leap in the dark at the moment. How can a typical student be
  better supported during development, how can he/she get into the topic faster, and see errors better?
  What automation can be provided for deployment? This feature will research typical requirements for
  development environments in microservice projects, and do s survey of Dungeon developers (and former
  participants...). This should lead to an improved concept (and at least partial implementation) of a
  development environment for the players. This include a "development strategy" in the documentation,
  to support player development, so that you have a guideline right at the beginning.
---
### What did we do?
- Write a development guide for new players (should minikube get involved?)
- overview of events, when and which are thrown (new images in static)
- videos of player, not too long
- Error and question page with possible solutions

### What we need to do!
- finish requirements for development environment for microservice architecture
- topic technical means? Unsure about specifications
- Another video for player insides and some features of existing player
- (Maybe markdown checkup)

### Problems or questions
- technical means. We might have problems implementing those (kubefwd, kubeVPN). What's expected of us exactly?
- Does gitlab block the linked videos. Does it work in the documentation?
