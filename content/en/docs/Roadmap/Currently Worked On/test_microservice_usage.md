---
title: "Test Microservice Usage"
linkTitle: "Test Microservice Usage"
layout: "newfeature/single"
# --- specific part
author: Matia Leon Schumacher
implementedDuring: WASP II "Microservices und Event-getriebene Architektur" (MEA - Grundlagenteil), SS2024, as WPF replacement
status: ongoing
repositories:
- repoUrl: https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-java-springboot
  branch: main
  repoChanges: test-cases & "mock"-profile
- repoUrl: https://gitlab.com/the-microservice-dungeon/docs
  branch: main
  repoChanges: mock-service docu
- repoUrl: https://gitlab.com/the-microservice-dungeon/test-services/mock-service
  branch: main
  repoChanges: new test-scenario
lastUpdate: 2024-08-10
description: >
  Continued development of the mock service as testing framework, its usage for player developers and TODOs related to that. The aspects I worked on are:

  A presentation of the mock service and its usage (finished/presented).

  Development of test-cases in the java skeleton fully utilizing the testing functionalities the mock service (finished, reviewed & merged).

  Development of the "mock"-profile in the java skeleton for mock testing purposes (finished, reviewed & merged).

  A presentation of the test-cases in the java skeleton and its usage (finished/presented).

  Reworking certain aspects of the mock documentation to fit the new contribution guidelines (finished, reviewed & merged).

  Expanding the functionalities of the mock service (not yet finished).

---

(tbd)
