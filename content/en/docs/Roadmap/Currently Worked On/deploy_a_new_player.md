---
title: "Access to the Rancher cluster and deployment of players"
linkTitle: "Deployment of players"
layout: "newfeature/single"
# --- specific part
author: Adrian Nierstenhöfer
implementedDuring: Praxisprojekt Informatik Bachelor
status: finished
repositories:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/devops-team/terraform
    branch: player-repos
    repoChanges: new player and own project
  - repoUrl: https://gitlab.com/the-microservice-dungeon/devops-team/terraform
    branch: player-repos-doku -> schon auf main
    repoChanges: documentation on how to add a new player to the cluster -> Komplette ReadMe bis auf den Abschnitt Secret
  - repoUrl: https://gitlab.com/the-microservice-dungeon/docs/-/blob/devops-player-repos-own-env/content/en/docs/Infrastructure/Devops%20Guide/Management/opentofu.md?ref_type=heads
    branch: devops-player-repo-own-env
    repoChanges: the complete documentation for opentofu
lastUpdate: 2024-10-10
description: >

---
# Main project:

The project aims to deploy a player onto the Rancher cluster. This involves creating a personal access account for the cluster.

# Below, I describe my past tasks in this project:

- Started the MSD project at the beginning of the summer semester 2023
- Assigned to the Admin group together with Maik, with Fijola assigning us tasks
- The main tasks in the first semester were getting familiar with "K3s," "K8s," and the "Goedel Cluster", as well as understanding the current MSD environment
- This phase ended with a presentation by Maik and me on the basics (Cube CTL commands)
- Various experiments, ranging from small to larger ones, were conducted with the cluster, all orchestrated by Fijola
- After the Code Fight, the goal was to work on a specific topic to complete our individual projects. The idea was to create a script that could restore the entire cluster. However, this turned out to be a short-term solution.
- At the beginning of 2024, it became clear that we could use Terraform. Fijola set up Terraform.
- I then took on the final project of automating the deployment of players.
- Additionally, I have written the documentation for the OpenTofu (currently still referred to as Terraform) page, which includes only the key points. The more detailed documentation has been stored in the DevOps Guide, where both the code and the general handling of OpenTofu are explained.
