---
title: "Real Time MSD"
linkTitle: "Real Time MSD"
layout: "newfeature/single"
# --- specific part
author: Robin Lemmer, Tom Vollmer
implementedDuring: WASP II "Microservices und Event-getriebene Architektur (MEA), SS2024, as topic (A2)
status: ongoing
repositories:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/game
    branch: RealTime-MSD
    repoChanges: Removed round system and command forwarding to other services and updated open and async api
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/robot
    branch: RealTime-MSD
    repoChanges: Added direct command acceptation, task locking and new robot capabilities
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/trading
    branch: RealTime-MSD
    repoChanges: Added direct command acceptation and added the new capabilities
  - repoUrl: https://gitlab.com/the-microservice-dungeon/player-teams/standard-players/player-monte
    branch: RealTime-MSD
    repoChanges: fitted the player to work in the realtime MSD
  - repoUrl: https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard
    branch: RealTime-MSD
    repoChanges: Changed game creation and round display to game time
lastUpdate: 2024-09-10
description: >
  Cycling by rounds is "unnatural" for microservice landscapes. In eCommerce system, as an example,
  this is unknown. It leads to unnecessary waiting times on the one hand, and "lagging players" on the other.
  Changing this means that events and commands will be processed in real time.
  In this feature, the game service will only control general game information - e.g. creating, starting and ending a game - and not anymore
  be a manager of a "game loop" or endpoint for commands. There is a throttling for the commands - one command per robot
  every few seconds. The timeframes are dynamic and up to game-balance. For the strategy of the players and the execution of
  the commands, this means "first come, first serve".
---

## Table of Contents
1. [Overview](#overview)
   1. [Old Architecture](#old-architecture)
   2. [New Architecture](#new-architecture)
2. [Planed Changes](#planed-changes)
   1. [Event Changes](#event-changes)
   2. [Command Responsibility](#command-responsibility)
   3. [Action Duration](#action-duration)
   4. [Robot Upgrades](#robot-upgrades)
      1. [New Upgrades](#new-upgrades)
      2. [Upgrade Changes](#upgrade-changes)
      3. [Upgrade Values](#upgrade-values)
   5. [Dashboard](#dashboard)
   6. [Docs](#docs)
   7. [Checklist](#checklist)
   8. [TODO](#todo)

### Overview

Before going into detail what changes have to be made, we want to emphasize the architectural difference we get by removing rounds from the game

#### Old Architecture


![The old/current Architecture](/images/realtimeMSD/old_architecture.png)


In this architecture the player communicates directly only with the game service, which controls the rounds and forwards the commands to the different services


#### New Architecture


![The new Architecture](/images/realtimeMSD/new_architecture.png)


With this new architecture we remove the game service as a gateway point for commands, which reduces the overall responsibility of the service and now only performs general game management -
e.g. creating, starting and ending games. Commands used whilst in game are now send directly to the corresponding service - all trading related commands to the trading-service and everything else to the robot-service.
The robot-service assumes the position of controlling that robots can only perform one action at a time.


## Planed Changes


### Event Changes


| Service | Old Event           | New Event             | Change                                                             |
|---------|---------------------|-----------------------|--------------------------------------------------------------------|
| Game    | RoundStatusEvent    |                       | Removed                                                            |
|         | GameStatusEvent     |                       | Game Timer ends the game                                           |
| Trading | TradeblePricesEvent |                       | One time full list of all prices at game start                     |
|         |                     | PriceChangedEvent     | Only contains price changes                                        |
| Robot   | RobotsRevealedEvent |                       | Only contains robots on the same planet                            |
|         |                     | ActivityDetectedEvent | Informs players with robots on a planet when activity was executed |
|         | RobotAttackedEvent  |                       | Interrupts targets action                                          |


### Command Responsibility


With the removal of the rounds we don't need to keep the game service as a command controller.
The Command responsibilities are now as followed:


| Service   | Commands    |
|-----------|-------------|
| Robot     | Battle      |
|           | Mining      |
|           | Movement    |
|           | Regenerate  |
| Trading   | Buying      |
|           | Selling     |


### Action Duration


In order to keep a balanced game experience we introduce action times. Those times determine how long it takes until the action is completed.


| Action       | Time  |
|--------------|-------|
| Fighting     | 3 Sek |
| Mining       | 3 Sek |
| Movement     | 3 Sek |
| Regenerate   | 3 Sek |
| Upgrade      | 3 Sek |


Timings are currently only temporary and are subject to change in order to improve the game balance.


### Robot Upgrades


Now that action are not round bound anymore and have a completion time we can add new robot upgrades and change how some upgrades work to be more logically correct


#### New Upgrades


- Movement Speed: reduces travel time between planets
- Regeneration Speed: reduces regeneration time
- Mining Efficiency: replaces the old mining speed upgrade
- Attack Speed: reduces attack time


#### Upgrade Changes


- Mining Speed: reduces mining speed


#### Upgrade Values

| Upgrade              | Effect                      | Level 0 | Level 1 | Level 2 | Level 3 | Level 4 | Level 5 |
|----------------------|-----------------------------|---------|---------|---------|---------|---------|---------|
| Attack Speed         | Percentage of action time   | 100 %   | 50 %    | 33 %    | 25 %    | 20 %    | 16 %    |
| Mining Speed         | Percentage of action time   | 100 %   | 50 %    | 33 %    | 25 %    | 20 %    | 16 %    |
| Movement Speed       | Percentage of action time   | 100 %   | 50 %    | 33 %    | 25 %    | 20 %    | 16 %    |
| Regeneration Speed   | Percentage of action time   | 100 %   | 50 %    | 33 %    | 25 %    | 20 %    | 16 %    |
| Mining Efficiency    | Mining Amount               | 2       | 5       | 10      | 15      | 20      | 40      |


### Dashboard


The dashboard needs a small change. Instead of displaying the current round it now has to display the current game time.


### Docs


The open and async api needs change with the new command responsibilities.


### Checklist


- [x] Removed Rounds
- [x] Change Command Responsibility
- [x] Robot Locking
- [x] Action Times
- [x] Robot Upgrades
- [ ] Updated docs (open/async api):
  - [x] game
- [ ] Tests


### TODO

The following things still need implementation or some changes:

- automatic unlocking of robot after a certain time has passed (in case of processing errors)
- attacking a robot will interrupt the targets action
- event for interrupted action
- ActivityDetectedEvent
- PriceChangedEvent (needs trading economics)
- fixing the dashboard (needs changes in dashboard-backend (rust))
- change the skeletons to work in the realtime msd
- update remaining docs (open and async api)
