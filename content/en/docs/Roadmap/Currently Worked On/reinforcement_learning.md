---
title: "Reinforcement Learning"
linkTitle: "Reinforcement Learning"
layout: "newfeature/single"
# --- specific part
author: Leon Püschel
implementedDuring: Praxisprojekt Informatik Bachelor
status: finished, but not yet reviewed
repositories:
  - repoUrl: https://gitlab.com/the-microservice-dungeon/player-teams/reinforcement-learning
    branch:
    repoChanges:
lastFileMod: 2024-07-5
description: >
  The aim of this project is to train a neural network that can take over the decisions about the actions for players of the MSD. To achieve this, I use reinforcement learning.
---

This [README](https://gitlab.com/the-microservice-dungeon/player-teams/reinforcement-learning/reinforcment-learnig-doku) should function as an entrypoint to this project. I recommend reading this through for a broad overview and then reading the readmes from the single components to get a better understanding of what they are doing.
