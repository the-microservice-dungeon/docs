---
title: "Test Microservice Framework"
linkTitle: "Test Microservice Framework"
layout: "newfeature/single"
# --- specific part
author: Matia Leon Schumacher
implementedDuring: Praxisprojekt, WS2023/24
status: ongoing
repositories:
- repoUrl: https://gitlab.com/the-microservice-dungeon/docs
  branch: main
  repoChanges: mock-service documentation
- repoUrl: https://gitlab.com/the-microservice-dungeon/test-services/mock-service
  branch: main
  repoChanges: mock-service implementation
lastUpdate: 2024-08-10
description: >
  Development of a testing service called 'mock-service' as a testing framework for player developers to test their player-services with both during and after development.
  The mock-service mainly offers support for integration and system tests. It offers a set of event generators that mock events of the 25 available event types with configurable values as well as a number of configurable test-scenarios that allow the player devs to test their players capability to handle various in-game mechanics of the actual game.
  The concrete functionality and usage of the mock-service has been documented in the microservice-dungeon documentation under: references -> test -> mock-service.

---

(tbd)
