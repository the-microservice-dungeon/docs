---
title: "Roadmap"
linkTitle: "Roadmap"
weight: 5
description: >
  There is (as of now) no roadmap for the Microservice Dungeon. However, there are some ideas for further
  development. In addition, there are always features that are currently worked on in student projects or thesis works.
---

