---
title: "Ideas for Further Development"
linkTitle: "Ideas for Further Development"
weight: 5
description: >
  This page lists ideas for further development of the Microservice Dungeon. As of now, this is a loose and not-yet-discussed
  collection of ideas. If you have any ideas, feel free to add them here.
---

## Improvements

**Streamlining API Documentation**

In the beginning of the project we followed an API-first approach where APIs where specified as OpenAPI / AsyncAPI first.
However, as things went on and we further improved the APIs there was a regular chance of the documentation being outdated as we now always have to adjust the Specification files according to our changes.
This has been proven to be an annoying and unsatisfactory experience.
It would be great to streamline the process so a change in the APIs is directly reflected into the documentation.
A few inputs for that:
- Generate OpenAPI specification (either during run- or buildtime) and/or migrating to a schma-driven approach (JSON Schema, Protocl Buffers, Avro, Thrift) in the asychnronous communication. Maybe introducing a schema registry (Apicurio, Buf).
- Testing APIs against specifications/contracts using [Microcks](https://microcks.io/), [Pact](https://docs.pact.io/) or other contract testing
- Develop a process (for example using code reviews and Merge/Pull Requests) in which every non-trival chang is being reviewed by another party
- Or maybe another reasonable solution?

**More sophisticated use of routing keys in RabbitMQ**

We're transporting player-relevant Events from Kafka to RabbitMQ by utilizing routing keys as simple identifiers for a player.
So every message for a player with ID `20bdbc57-f1f7-4f99-a3e6-9e2db042c732` will receive the exact same ID as a routing key.
However, the Routing Key offers a way more sophisticated way to use. For example using the scheme:
`dungeon.<PLAYER>.<ROBOT>.<EVENT>` a player can create lightweight queues to filter for specific events of a robot. For example with the binding key `dungeon.hackschnitzel.*.RobotKilled` i would receive all robot killed events and am able to build a state-handler based on that.
RabbitMQ allows creating Permissions on Routing Keys so we can ensure that a player isn't using a binding key to "steal" events from other players
