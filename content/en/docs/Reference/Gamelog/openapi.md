---
title: "OpenAPI"
linkTitle: "OpenAPI"
weight: 9
type: swagger
description: >
  OpenAPI
---

{{< swaggerui src="https://gl.githack.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/docs/api-spec.yaml" >}}
