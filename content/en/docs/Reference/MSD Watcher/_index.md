---
title: "MSD Watcher"
linkTitle: "MSD Watcher"
weight: 8
description: >
    The MSD watcher / monitor
---

{{% pageinfo %}}
This site is more of a description than a technical documentation.

This is because **this Project is highly unfinished!** Most features are in a very rough state and some are present, but
not properly integrated (i.e. auth using RBAC)! It is not in a state where a full reference documentation is even possible!\
**_Handle with care_**
{{% /pageinfo %}}


# Concept
Provide a tool which visualizes every `technical` thing of the MSD. This means:
- The (deployed) kubernetes landscape
    - Pods / Replicas / Services
    - Namespaces
    - Ingress / Endpoints
- Stats
    - Load
    - Execution Time
    - Health
- Visualize internal communication
    - Pods that emmit events should pulse or blink
    - Maybe even rest calls from pod to pod that can be visualized using an animated line in the graph
        - This might be irrelevant as we move to an exclusively event based communication model

# Project Structure
This project consists of two maven packages. \
One (`backend`) contains a [Quarkus](https://quarkus.io/) java project that
handles all backend / server tasks. It interfaces with the kubernetes api, provides a REST api for the frontend and also
acts as the web server that serves the frontend. \
The other (`frontend`) contains a [Vue 3](https://vuejs.org/) project
with [typescript](https://www.typescriptlang.org/) and is responsible for visualizing the data provided by the backend.

## Backend
The backend project exposes a few simple api endpoints for the frontend which interface with the kubernetes api (and in
the future kafka) and clean up that data to a more workable format.
It also acts as the webserver that serves the frontend.

This project is written in Java and utilises the [Quarkus](https://quarkus.io/) framework.

For further information and documentation see the [backend documentation](/docs/reference/msd-watcher/backend/)!


## Frontend
The frontend project acts as the main UI for the msd watcher. It visualizes the deployed kubernetes infrastructure in a
graph, and is supposed to show activity (events, internal rest calls, etc.) too in the future.

This project is written in [Vue.js 3](https://vuejs.org/) using [vite](https://vitejs.dev/) and [typescript](https://www.typescriptlang.org/).

For further information and documentation see the [frontend documentation](/docs/reference/msd-watcher/frontend/)!

