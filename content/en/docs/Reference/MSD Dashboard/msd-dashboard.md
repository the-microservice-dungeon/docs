---
title: "MSD Dashboard"
linkTitle: "MSD Dashboard"
description: >
  MSD Dashboard
---

{{% alert title="Hint" %}}

This following part of the documentation is embedded from the README of the
[msd-dashboard repository](https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard).
If you experience any issues on this page, just visit the repository directly.

{{% /alert %}}

{{% gitlab-md-file "https://gitlab.com/api/v4/projects/53524510/repository/files/README%2Emd?ref=main" %}}