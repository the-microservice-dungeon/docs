---
title: "Robot Service"
linkTitle: "Robot Service"
weight: 3
description: >
  Robot Service
---

# Robot Service Technical View

## Robot and its Information

The robot has several variables that you must keep an eye out for.

You can the information for your robot by using this REST call:

>**GET**
```
http://{defaultHost}/robots/{robot-uuid}
```

>**Response Payload Example**
```json
{
  "id": "497f6eca-6276-4993-bfeb-53cbbbba6f08",
  "player": "ae2cfcf0-e870-4360-a41e-3b3bb3312234",
  "planet": "2faf337d-d8d1-40fc-983e-5f130540496b",
  "alive": true,
  "maxHealth": 100,
  "maxEnergy": 60,
  "energyRegen": 8,
  "attackDamage": 5,
  "miningSpeed": 10,
  "health": 75,
  "energy": 43,
  "healthLevel": 5,
  "damageLevel": 5,
  "miningSpeedLevel": 5,
  "miningLevel": 5,
  "energyLevel": 5,
  "energyRegenLevel": 5,
  "storageLevel": 5,
  "inventory": {
    "maxStorage": 20,
    "usedStorage": 5,
    "coal": 3,
    "iron": 2,
    "gem": 0,
    "gold": 0,
    "platin": 0
  }
}
```

## Robot Data


* **general**: `id`, `player`, `planet`, `alive(y/n)` -> These are self-explanatory.

* **Max stats** of robot according to current upgrade status: `maxHealth`, `maxEnergy`, `energyRegen`, `attackDamage`, `miningSpeed` -> These are variables that can be improved by upgrades bought by the trading service.

* **Current status** of the robot: `health`, `energy` -> your current pool of helath and energy. A Robot is not dying with "0" Energy. You simply cannot use another action exept of `regenerating`.

* **Current upgrade level**: healthLevel, damageLevel, miningSpeedLevel, miningLevel, energyLevel, energyRegenLevel, storageLevel -> you can only upgrade to the next level via trading service.


* **object “inventory” ** with attributes: `maxStorage`, `usedStorage`, `storedCoal`, `storedIron`, `storedGem`, `storedGold`, `storedPlatin`

## Spawning a Robot

The spawn of the robot is a direct result of a "Buy robot command" to the trading service.

If a player has enough money the trading service uses a REST call to the robot service.

## Upgrade transaction

* Game service: issues the command which was received by the player to the trading service
* Trading service: processes the command, checks if position is a space station, checks if player has enough money, withdraw money
* Robot service: validate robot and do the upgrade

The robot service only has to validate the given robot and check if the bought upgrade is possible.

## Service-oriented Functions

- [OpenAPI](/docs/reference/robot/openapi/)
- [AsyncAPI](/docs/reference/robot/asyncapi/)


## Repository Link Robot

[Repository Link Robot](https://gitlab.com/the-microservice-dungeon/core-services/robot)


