---
title: "Tradeables"
linkTitle: "Tradeables"
weight: 2
description: >
  Tradeables
---

# Tradeables

Tradeables are Item, Resource, Restoration or Upgrade. They have an Name, Price and Type.

## Resources

### Resources types

Before you will be able to afford more than just your starting robot you will have to mine resources.

There are five resource types, which can be found on the planets.
These are the starting selling prices for the resources. 

| Value  | Name   | Price |
| ------ | ------ | ----- |
| COAL   | Coal   | 5     |
| IRON   | Iron   | 15    |
| GEM    | Gem    | 30    |
| GOLD   | Gold   | 50    |
| PLATIN | Platin | 60    |

## Items

| Value | Name  | Description        | Price |
| ----- | ----- | ------------------ | ----- |
| ROBOT | Robot | Buys another Robot | 100   |

## Restorations
 
| Value          | Name               | Description                   | Price |
| -------------- | ------------------ | ----------------------------- | ----- |
| HEALTH_RESTORE | Health restoration | Heals the robot to full HP    | 50    |
| ENERGY_RESTORE | Energy restoration | Restores robot to full Energy | 75    |

## Upgrades

Upgrades improve the variables of your robot. For example, a bigger health pool. 

### Upgrade types

| Value          | Description                         |
| -------------- | ----------------------------------- |
| STORAGE_N      | Storage Level N=1-5 Upgrade         |
| HEALTH_N       | Health Points Level N=1-5 Upgrade   |
| DAMAGE_N       | Damage Points Level N=1-5 Upgrade   |
| MINING_SPEED_N | Mining Speed Level N=1-5 Upgrade    |
| MINING_N       | Mining Strength Level N=1-5 Upgrade |
| MAX_ENERGY_N   | Energy Capacity Level N=1-5 Upgrade |
| ENERGY_REGEN_N | Energy Regen Level N=1-5 Upgrade    |

### Upgrade Prices

| Level | Price |
| ----- | ----- |
| 1     | 50    |
| 2     | 300   |
| 3     | 1500  |
| 4     | 4000  |
| 5     | 15000 |

### Upgrade Restriction

There are two restrictions, when it comes to buying `upgrades`:

* You can only buy **one** `upgrade` per robot in one round. The reason for this is that an upgrade is seen as a single action. Just imagine it as giving your car to the shop for a tuning.

* You can only buy an upgrade to the next level of the variable, you want to improve. For example, you only can upgrade your **HEALTH_1 to HEALTH_2**.