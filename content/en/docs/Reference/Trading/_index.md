---
title: "Trading Service"
linkTitle: "Trading Service"
weight: 4
description: >
  Trading Service
---


# Trading Service Technical View

## Lifecycle Technical information

- Trading keeps a bank account for every player
- Trading debits bank account based on trading operation (Buy/Sell)
- Trading doesn't care about game mechanics, it will happily sell you an upgrade for a non-existing robot and charge your account
- Trading announces prices at the start of each round

## Player and Money

Trading saves a `Player`. The playerId is known, by listening to the Player registered event of the game service.

Trading is also the only service which saves the money of the registered `Player` as an attribute Money which is a numeric value.

## Events for player

### BankAccountInitialized

This event is produced when a player bank account is initialized.

### BankAccountCleared

When a bank account has been cleared (at the end of a game).

### BankAccountTransactionBooked

When the bank account has been charged.

### TradablePrices

At the start of each round prices are announced

### TradableSold

When something has been sold

### TradableBought

When something has been bought


## Service-oriented Functions

- [OpenAPI](/docs/reference/trading/openapi/)
- [AsyncAPI](/docs/reference/trading/asyncapi/)
