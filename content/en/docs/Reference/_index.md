---
title: "API Reference"
linkTitle: "API Reference"
weight: 4
description: >
  This page lists all the API documentation for the Microservice Dungeon. This includes synchronous (REST) and
  asynchronous (events) communication.
---
