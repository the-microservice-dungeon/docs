---
title: "Commands"
linkTitle: "Commands"
weight: 2
description: >
  Commands
---


# Commands

The game service must manage the commands from the player because it owns the information about the round timer and can send the commands at the right time to the right services.

It owns a list of all robots, to verify, that players don’t sent commands for robots of other players.

The list is created by the information of the robot-create-events, so the service knows all the robots, that are owned by the players.

The commands from the players are stored in the game service and are sent to the other services, that fit the command-type during the execution phase.

The players receives an id, to find the right event, that informs him of the outcome of his command.

How does a command look like?

>**example command:**
```json
{
  "type": "buying",
  "data": {
    "robotId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
    "itemName": "d290f1ee-6c54-4b01-90e6-d701748f0851",
    "itemQuantity": "d290f1ee-6c54-4b01-90e6-d701748f0851",
  },
}
```

>**response example**
```json
{
  "transactionId": "d290f1ee-6c54-4b01-90e6-d701748f0851"
}
```

