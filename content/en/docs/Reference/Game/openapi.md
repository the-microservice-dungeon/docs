---
title: "OpenAPI"
linkTitle: "OpenAPI"
weight: 9
type: swagger
description: >
  OpenAPI
---

{{< swaggerui src="https://gl.githack.com/the-microservice-dungeon/core-services/game/-/raw/main/doc/openapi-doc.yaml" >}}
