---
title: "OpenAPI"
linkTitle: "OpenAPI"
weight: 4
type: swagger
description: >
    OpenAPI
---

{{< swaggerui src="https://gl.githack.com/the-microservice-dungeon/mock-service/-/raw/main/docs/openapi.yaml" >}}
