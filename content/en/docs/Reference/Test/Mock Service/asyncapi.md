---
title: "AsyncAPI"
linkTitle: "AsyncAPI"
weight: 5
type: asyncapi
description: >
  AsyncAPI
---

The async api is purposefully intended to be exactly the same and equal to that of the core services. Therefore, please
refer, for the mock services async api, to the async api of the core services.

  - [robot](/docs/reference/robot/asyncapi/)
  - [map](/docs/reference/map/asyncapi/)
  - [game](/docs/reference/game/asyncapi/)
  - [trading](/docs/reference/trading/asyncapi/)

