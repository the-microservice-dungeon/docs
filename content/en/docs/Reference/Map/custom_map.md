---
title: "Custom map"
linkTitle: "Custom map"
weight: 4
description: >
  Describes the possible options for configuring custom maps
---

# Custom game world configuration

When creating a new game, clients have the option to customize the map layout and resource distribution when creating
a new game. Otherwise, a default map is generated.

In order to create a custom map, the `gameworldSettings` field has to be provided as part of the request body when
creating a new game at the game service. See `POST /games` for the game service for details. Below is an example
what a request body may look like.

> **Example request body for creating a new game**

```json
{
  "maxRounds": 58,
  "maxPlayers": 6,
  "gameworldSettings": {
    "map": {
      "type": "custom",
      "size": 5,
      "options": {
        "layout": "XOOXX;OOOOO;OXXOO;XXXXO;OOOOO"
      }
    },
    "resources": {
      "minAmount": 500,
      "maxAmount": 5000,
      "areaSettings": {
        "border": {
          "coal": 1.0
        },
        "inner": {
          "iron": 0.4,
          "gem": 0.1
        }
      }
    }
  }
}
```

As can be seen in the example, `gameworldSettings` is an object with two keys: `map` and `resources`.

- `map`: Settings that change the layout of the map.
- `resources`: Settings that change the distribution and richness of resources.

All of these settings are option. In case either or both `map` and `resources` are left out, the default generation
takes precedence.


## Map Settings

Map settings allow to shape the layout of the map. There are three fields:

- `type` sets the map type. A detailed explanation of all supported types can be found below. If left blank the service
  will default to type `default`.
- `size` allows to specify a map size and override the default behavior which calculates the map size based of the
  number of participating players.
- `options` is an additional map that contains options that strongly correlate with the map type. These
  are optional and explained for each map type below. Unsupported options are ignored. Note, that there is an exception
  for type `custom` that requires a valid layout as part of the options.


### Map types and options

#### 1. DEFAULT - `default`

The `default` map is chosen when `type` is left blank or no `gameworldSettings` are provided. It is randomly generated
(as all maps are) and covered with planets with some being black holes as obstacles in the `outer` and `mid` area of the
map.

**Supported Options:**
none

> **Example:**

```json
{
  "map": {
    "type": "default",
    "size": 10
  }
}
```

> **Example Output:**

|   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|
| O | O | O | O | O | O | O | O | O | O |
| O | X | O | O | O | O | O | X | O | O |
| O | O | X | O | X | X | O | O | O | O |
| O | O | X | X | O | O | X | O | O | O |
| O | O | O | X | O | O | O | O | O | O |
| O | X | X | O | O | O | O | X | O | O |
| O | O | O | O | O | O | X | O | O | O |
| O | O | X | O | X | O | O | O | O | O |
| O | O | X | X | O | O | X | X | X | O |
| O | O | O | O | O | O | O | O | O | O |

#### 2. CUSTOM

Type `custom` allows for a complete customization of the layout. This is done by providing a `layout` option.

**Supported Options:**

- `layout` (String): The layout is a string consisting of two characters `X` and `O` (the letter, not the numeral)
  with `;` being the delimiter. `X` represents a planet while `O` results in a black hole.

**Caution:**

- The length of each block (or row) must be equal. For instance `"XOO;OO;OO"` is invalid.
- The layout must be square and match the map size. It is therefore recommended to provide a fixed size for this type of
  map.

> **Example:**

```json
{
  "map": {
    "type": "custom",
    "size": 5,
    "options": {
      "layout": "XOOXX;OOOOO;OXXOO;XXXXO;OOOOO"
    }
  }
}
```

> **Example Output:**

|   |   |   |   |   |
|---|---|---|---|---|
| X | O | O | X | X |
| O | O | O | O | O |
| O | X | X | O | O |
| X | X | X | X | O |
| O | O | O | O | O |

#### 3. CORRIDOR

Type `corridor` creates a map consisting of two black holes that contain random rows and columns filled with planets ("corridors").
These corridors are cut off from each other, meaning that there can't be two corridors next to each other.

Note, that there is always at least one row or column with planets present. It is not possible to create a map that
consists only of black holes.

**Supported Options:**

- `frequency` (Numeric between 0 and 1): The probability for a corridor occurring in rows / columns.
- `width` (Numeric > 0): The width of each corridor. The default is 1.

> **Example:**

```json
{
  "map": {
    "type": "corridor",
    "size": 10,
    "options": {
      "frequency": 0.2,
      "width": 1
    }
  }
}
```

> **Example Output:**

|   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|
| O | O | O | O | O | O | O | O | O | O |
| X | O | X | X | X | X | X | X | X | X |
| X | O | X | X | X | X | X | X | X | X |
| X | O | X | X | X | X | X | X | X | X |
| X | O | X | X | X | X | X | X | X | X |
| O | O | O | O | O | O | O | O | O | O |
| X | O | X | X | X | X | X | X | X | X |
| X | O | X | X | X | X | X | X | X | X |
| X | O | X | X | X | X | X | X | X | X |
| X | O | X | X | X | X | X | X | X | X |

#### 4. ISLANDS

Type `islands` creates a "sea" of black holes with islands of planets in it.
Islands are clusters of planets that are isolated from each other, meaning that robots cant travel between them. These
islands are randomly placed and vary in size.

Internally the algorithm used to create this type of map is a flood fill algorithm.
It randomly chooses a starting planet using the `frequency` value from the list of yet unvisited planets. Islands are
created recursively by the algorithm by visiting all neighbouring planets and marking them as either a planet or a black
hole by using the `size` option.

**Supported Options:**

- `frequency` (Numeric between 0 and 1): Controls the amount of islands appearing on the map. The default is 0.5.
- `size` (Numeric between 0 and 1): Controls the island size. The default is 0.75.

> **Example:**

```json
{
  "map": {
    "type": "islands",
    "size": 10,
    "options": {
      "size": 0.6,
      "frequency": 0.2
    }
  }
}
```

> **Example Output:**

|   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|
| O | O | O | X | X | X | X | O | O | X |
| X | X | O | O | X | X | O | X | O | X |
| X | X | O | X | X | O | O | O | O | O |
| X | X | X | X | O | O | X | X | X | X |
| X | X | O | O | X | O | X | O | X | X |
| X | X | O | X | O | O | O | O | O | O |
| X | O | X | O | O | O | O | X | O | O |
| X | O | O | O | O | O | O | O | O | O |
| O | O | X | O | O | O | O | O | X | O |
| X | X | X | X | O | O | O | X | X | X |

#### 5. MAZE

Type `maze` creates a maze where the aisles are planets and the walls are black holes.
Strategically this means that there are longer distances to traverse for robots to arrive in certain areas. It is also
possible to flank opponents in a fight or trap fleeing opponents in a dead end etc.

A map with type maze is generated by choosing a starting planet and recursively branching out in all
directions marking its neighbours as visitable planets.
If the neighbour already has a neighbour that is a visitable planet, this planet will either be declared a wall or a
visitable planet using the `clusters` probability.

##### Options

- `clusters` (Numeric between 0 and 1): Controls the probability of "plazas"/clusters of adjacent planets
  appearing. Default is 0.02.

> **Example:**

```json
{
  "map": {
    "type": "maze",
    "size": 10,
    "options": {
      "clusters": 0.2
    }
  }
}
```

> **Example Output:**

|   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|
| O | O | O | O | O | O | O | O | O | O |
| O | X | O | X | X | X | X | O | X | O |
| O | O | O | O | O | O | O | X | O | O |
| O | X | O | O | O | X | O | X | O | O |
| X | O | O | X | O | O | O | X | O | X |
| O | O | O | X | X | O | O | O | O | O |
| O | X | O | O | X | O | X | O | X | O |
| O | O | O | O | O | O | X | O | O | O |
| O | O | X | O | O | X | O | X | X | O |
| O | O | O | O | X | O | O | O | O | O |

## Resources Settings

By default, resources are distributed as followed:

- `COAL` can be found in the `BORDER` and `OUTER` regions of the map with a probability of 80%.
- `IRON` can be found in the `MID` region of the map with a probability of 50%.
- `GEM` can be found in the `MID` region of the map with a probability of 30%.
- `GOLD` can be found in the `INNER` region of the map with a probability of 20%.
- `PLATIN` can be found in the `INNER` region of the map with a probability of 10%.
- The resource amount on planets is always 10,000 by default.

Using the `resources` settings, you have more control over the amount and placement of those resources.

- `minAmount`: Controls the minimum amount of resources on a planet.
- `maxAmount`: Controls the maximum amount of resources on a planet.
- `areas`: Enables a detailed specification on how each resource type is placed in each area by which probability.

As with all other options, these options are optional.

### `amount`-Settings

The amount of resources each deposit contains are a random number between the `minAmount` and `maxAmount`. Depending on
which variable is set the following behavior is to be expected:

| `minAmount` | `maxAmount` | Behavior                          |
|-------------|-------------|-----------------------------------|
| Value A     | Value B     | Random value between A and B      |
| null        | Value B     | Random value between 0 and B      |
| Value A     | null        | Random value between A and 10.000 |
| null        | null        | Default of 10.000                 |


### `area`-Settings

The `areaSettings` enables detailed control over which resource type is placed in which area by which probability.
Therefore, the setting is a nested map by [area](#map-areas) and resource with a probability between 0 and 1.

**Caution:** Providing an area setting overrides the default placement entirely. Meaning a game world with the following
setting would have no resources in the `MID` and `OUTER` areas.

> **Example:**

```json
{
  "areaSettings": {
    "border": {
      "coal": 1.0
    },
    "inner": {
      "platin": 0.3,
      "gold": 0.5
    }
  }
}
```

### Map areas

![Map Areas](/images/map/map_areas.png)

There are four map areas:

- The `BORDER` (red) region contains all the planets at the edge of the map
- The `INNER` (yellow) region contains all the planets that are more than `(size - 1) / 3` planets away from the edge.
- The `MID` (green) region contains all the planets that are more than `(size - 1) / 6` planets away from the edge.
- The `OUTER` (blue) region contains all the planets not in any of the above regions.

For the example above the `INNER` region is 3 planets away from the edge (`(10 - 1) / 3 = 3`) and the `MID` region 1
planet away from the edge (`(10 - 1) / 6 = 1` because of integer division).

Therefore, a map with a size of 50 would have an `INNER` region that starts after `(50 - 1) / 3 = 16` planets away
from the edge and a `MID` region that starts after `(50 - 1) / 6 = 8` planets.
