---
title: "Map Service"
linkTitle: "Map Service"
weight: 2
description: >
  Map Service
---

# Map Service Technical View

## Map Size

The map size depends on the number of players. For less than 10 players the map size is 15x15, for more than 10 the size
is 20 and above that the size is equal to the number of players. But there is an option when starting a new game to
override this behavior with a specific size.

## Map Structure

The map (also called the gameworld) consists of fields, referred to as planets. Each planet is located on the map by
two coordinates (x and y) with neighbouring planets and depending on its position have a movement difficulty that
influences the amount of energy needed in order to move a robot. There are four different areas: inner, mid, outer and
border. Usually, planets in the inner part of the map have the highest difficulty with a difficulty of 3, whereas
planets in the middle and outer part have a difficulty of 2 and 1.

The area also effects which type of resource a planet may contain. While the outer area only contains coal, planets
in the middle area may contain iron and gem deposits and in the most inner area gold and platin.

Additionally, some planets in the outer area have a space station. Space stations are locations at which trading takes
place, new robots spawn and where robots have a higher regeneration rate in general. Lastly, not every field on the map
is a planet a robot can move to. Some fields are blank and represent an obstacle. They are called a black holes.


## Service-oriented Functions

- [OpenAPI](/docs/reference/map/openapi/)
- [AsyncAPI](/docs/reference/map/asyncapi/)

## Repository Link Map
[Repository Link Map](https://gitlab.com/the-microservice-dungeon/core-services/map)




