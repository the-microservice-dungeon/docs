---
title: "OpenAPI"
linkTitle: "OpenAPI"
weight: 9
type: swagger
description: >
  OpenAPI
---

{{<swaggerui src="https://gl.githack.com/the-microservice-dungeon/core-services/rs-microservice-dungeon-map-api-backend/-/raw/master/doc/openapi-doc.yaml">}}