---
title: "Introduction"
linkTitle: "Api Introduction"
weight: 1
description: >
  Introduction to the Map Data Api
---


# Intoduction

The purpose of the map data api specification is to provide a standardised way to provide map related data for frontend applications.

# Data Types

Data Types are oriented to be the intersect of **Kafka** and **Player** Data and Events.
All basedatatypes can be derived by **Kafka** and **player** events.
Optional data derivable by **Kafka** and **player** events are treated as normal value of planet or robot.
Optional data defined per **api source** by are passed down in form of **Traits** for every planet or robot.


## Planets

```ts
{
    planetId: UUID
    gameId: UUID | undefined
    gameworldId: UUID | undefined
    neighbours: Array<Neighbour>
    resourceType: string | undefined
    resource: Resource | undefined
    movementDifficulty: number
    traits: Array<Trait>
}
```

### Neighbour
```ts
{
    direction: string
    id: UUID
}
```

### Resource
```ts
{
    amount: number
    capacity: number
}
```

## Robots

```ts
{
    robotId: UUID
    gameId: UUID | undefined
    gameWorldId: UUID | undefined
    planetId: UUID
    // Can be a player Notion or UUID
    playerId: string
    vitals: Vitals | undefined
    levels: Levels | undefined
    cargo: Cargo | undefined
    traits: Array<Trait>
}
```

### Vitals

```ts
{
    health: number
    energy: number
}
```

### Levels

```ts
{
    health: number,
    energy: number,
    miningLevel: number,
    miningSpeed: number,
    damage: number,
    energyRegeneration: number,
    storage: number
}
```

### Cargo

```ts
{
    capacity: number
    used: number | undefined
    free: number | undefined
    coal: number
    iron: number
    gem: number
    gold: number
    platin: number
}
```

### Traits

for Traits refer to [Traits](../traits/trait_structure/)

# Examples

## Planets:

```json
[
{
        "planetId": "01ed605c-f53d-49c8-b052-73ea016dacf3",
        "gameworldId": "bece4219-7e31-480f-917f-21532b5fbe93",
        "neighbours": [
            {
                "direction": "NORTH",
                "id": "0a178693-635e-49e2-af0c-823cb84e61f4"
            },
            {
                "direction": "SOUTH",
                "id": "3ba59b3a-2c96-4b14-b23d-685fc88f2858"
            }
        ],
        "resourceType": "COAL",
        "resource": {
            "amount": 10000,
            "capacity": 10000
        },
        "movementDifficulty": 0,
        "traits": [
            {
                "trait": "position",
                "context": "publicApi",
                "data": {
                    "position": {
                        "x": 0,
                        "y": 0,
                        "z": 0
                    }
                }
            }
        ]
    },
    {
        "planetId": "0a178693-635e-49e2-af0c-823cb84e61f4",
        "gameworldId": "bece4219-7e31-480f-917f-21532b5fbe93",
        "neighbours": [
            {
                "direction": "SOUTH",
                "id": "01ed605c-f53d-49c8-b052-73ea016dacf3"
            }
        ],
        "movementDifficulty": 0,
        "traits": [
            {
                "trait": "position",
                "context": "publicApi",
                "data": {
                    "position": {
                        "x": 0,
                        "y": 1,
                        "z": 0
                    }
                }
            }
        ]
    },
    {
        "planetId": "01ed605c-f53d-49c8-b052-73ea016dacf3",
        "gameworldId": "bece4219-7e31-480f-917f-21532b5fbe93",
        "neighbours": [
            {
                "direction": "NORTH",
                "id": "0a178693-635e-49e2-af0c-823cb84e61f4"
            },
            {
                "direction": "SOUTH",
                "id": "3ba59b3a-2c96-4b14-b23d-685fc88f2858"
            }
        ],
        "resourceType": "COAL",
        "resource": {
            "amount": 10000,
            "capacity": 10000
        },
        "movementDifficulty": 0,
        "traits": [
            {
                "trait": "position",
                "context": "publicApi",
                "data": {
                    "position": {
                        "x": 2,
                        "y": 12,
                        "z": 0
                    }
                }
            }
        ]
    },
]
```

## Robots:

```ts
[
    {
        "robotId": "0a178693-f53d-2c96-b23d-685fc88f2858",
        "gameWorldId": "bece4219-7e31-480f-917f-21532b5fbe93",
        "planetId": "01ed605c-f53d-49c8-b052-73ea016dacf3",
        // Can be a player Notion or UUID since Players retrieve only a shortened PlayerId for other Players
        "playerId": "bece4219-b052-49c8-f53d-73ea016dacf3",
        "vitals": {
            health: 20,
            energy: 20
        },
        "levels": {
            health: 0,
            energy: 0,
            miningLevel: 0,
            miningSpeed: 0,
            damage: 0,
            energyRegeneration: 0,
            storage: 0
        },
        "cargo": {
            capacity: 20,
            used: 20,
            free: 0,
            coal: 20,
            iron: 0,
            gem: 0,
            gold: 0,
            platin: 0
        },
        "traits": []
    }
]
```
