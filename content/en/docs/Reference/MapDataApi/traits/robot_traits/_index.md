---
title: "Robot Traits"
linkTitle: "Robot Traits"
weight: 1
description: >
    Trait Guide
---


# Simple Traits

## Hostile

Marker for robots considered to be **hostile** / enemies.

```ts
{
    trait: "hostile"
}
```

## Friendly

Marker for **friendly** robots.  
May apply for other players robots (e.g. Aliance).


```ts
{
    trait: "friendly"
}
```

# Target Traits

# Complex Traits

## Role Trait

Role of the robot.

**primaryRole** describes the **main role** of the **robot**.  
**secondaryRole** describes the **secondary Role** of the Robot if needed.  
example use of secondaryRole would be **commander**, a role that commands other robots.  

Sample Roles:

- "miner"
    - Mining of resources
- "fighter"
    - Fighting other robots
- "scout"
    - Exploring the map
- "commander"
    - Commanding other robots


```ts
{
    trait: "role",
    data: {
        primaryRole: string
        secondaryRole: string | undefined
    }
}
```

## Roles

Similar to **Role** if a robot has multiple roles it fullfils 

```ts
{
    trait: "roles",
    data: {
        roles : string[]
    }
}
```

## State Trait

Defines the current actions a robot is doing

sample states:

- "mining"
- "fleeing"
- "trading"
- "upgrading"
- "moving"

```ts
{
    trait: "state",
    data: {
        state : string
    }
}
```

## Status

Current Status Information of the robot

sample Status:

- "fullCargo"
- "lowHealth"
- "lowEnergy"

```ts
{
    trait: "status",
    data: {
        status: string
    }
}
```

# Example Usage on how Robot Traits can be used

## Hostile / Friendly

Different contexts can be used to determine which Robots are considered as friendly or hostile from multiple perspectives.


```json
[
    {
        "trait": "hostile",
        "context": "Player1"
    },
        {
        "trait": "friendly",
        "context": "Player2"
    },
    {
        "trait": "hostile",
        "context": "Player1_priority_attack_targets"
    },
]
```

## Role / Roles

Roles can be used depending on the data to visualize Robots differently. Example usages is to have different Models depending on the combination of Roles, or just as simple to highlight those through a search Option

```json
{
    "trait": "role",
    "data": {
        "primaryRole": "miner",
        "secondaryRole": "commander"
    }
}
```

```json
{
    "trait": "roles",
    "data": {
        "roles": [
            "miner",
            "commander"
        ]
    }
}
```

