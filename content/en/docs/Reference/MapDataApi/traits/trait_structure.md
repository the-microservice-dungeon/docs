---
title: "Trait Structure"
linkTitle: "Trait Structure"
weight: 1
description: >
    Generalised structure of Traits
---

# Trait Categories

## Simple Trait

```ts
{
    // Optional value that represents the Trait Context if provided.
    // Possible usage is to show context specific data if multiple are provided
    context: string | undefined
    // Trait Type Identifier
    trait: string
}
```

Refer to
[PlanetTraits](../planet_traits/#simple-traits) ,
[RobotTraits](../robot_traits/#simple-traits)
for examples

## Target Trait

Reference to other Planets / Robots for a specific

```ts
{
    context: string | undefined
    trait: string
    // Selector to which Objects it References
    selector: "robot" | "planet"
    // List of the Ids that are referenced
    target: Array<UUID>
}
```

Refer to
[PlanetTraits](../planet_traits/#target-traits) ,
[RobotTraits](../robot_traits/#target-traits)
for examples

## Complex Trait

Traits that provide more Data that cant be provided as Simple Trait

```ts
{
    context: string | undefined
    trait: string
    // Additional Data provided
    data: TraitData
}
```

Refer to
[PlanetTraits](../planet_traits/#complex-traits) ,
[RobotTraits](../robot_traits/#complex-traits)
for examples

## Reserved Trait Types

Some trait types are standardised by the api specification and should not be used for custom trait types specified by third partie implementation

Refer to
[PlanetTraits](../planet_traits/) ,
[RobotTraits](../robot_traits/)
for a list of predefined traits


