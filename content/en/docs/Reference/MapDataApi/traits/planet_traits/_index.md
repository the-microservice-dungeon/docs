---
title: "Planet Traits"
linkTitle: "Planet Traits"
weight: 1
description: >
    Trait Guide

---


# Simple Traits

## Depleted

Planet with a **deposit** that has been **depleted** by **mining**.
Does not apply to planets that had no resource field at game start

```ts
{
    trait: "depleted"
}
```

## Discovered

Marker for a **planet** that has been discovered.
Exact definition of a **planet** that has been **discovered** may varie by implementation.

```ts
{
    trait: "discovered"
}
```

## Undiscovered

Marker for a **planet** that has not been discovered yet.
Exact definition of a **planet** that has been **discovered** may varie by implementation.


```ts
{
    trait: "undiscovered"
}
```

## Safe

Marker of a **planet** that is considered to be safe.
Exact definition of a **safe** planet may varie by implementation.

```ts
{
    trait: "safe"
}
```

## Unsafe

Marker of a **planet** that is considered to be safe.
Exact definition of a **safe** planet may varie by implementation.

```ts
{
    trait: "unsafe"
}
```

## Space Station

Marker of a **planet** that has a space station attached to it.
(May not be implemented yet or beeing unused)

```ts
{
    trait: "spaceStation"
}
```

# Target Traits

# Complex Traits

## Position

Defines the relative **position** of a **planet** for rendering purpose. (e.g. Map rendering in a grid layout)
Positions are relative to a layer if provided

```ts
{
    trait: "position",
    data: {
        position: {
            x: number
            y: number
            // technically not required yet but if needed in the future
            z: number | undefined
        }
    }
}
```

## Layer

Used to define a **layer** in which **clusters** of **planets** are defined.
A cluster of planets are planets, that can be reached from any **planet** in the same **cluster**.

```ts
{
    trait: "layer",
    data: {
        layer: string
    }
}
```


# Examples how data can be displayed :


## Simple Traits (here depleted Trait)

As simple Traits work as a label , they can only show 2 states depending of the data. They can be used to determine if an icon should show different version (e.g. grayscale).

simplefied example of it can be used:

![Depleted Trait Example](depleted_trait.png)

(planet containing ore and one having it depleted)


## Position Trait

simplified view of data using the Position Trait
in a global context

![Position Trait](position_trait.png)

<details> 
    <summary>Json</summary>

```json
{
    {
        "planetId": "1",
        "gameworldId": "1",
        "neighbours": [
            {
                "direction": "NORTH",
                "id": "2"
            },

        ],
        "resourceType": null,
        "resource": null,
        "movementDifficulty": 0,
        "traits": [
            {
                "trait": "position",
                "context": "publicApi",
                "data": {
                    "position": {
                        "x": 0,
                        "y": 0,
                        "z": 0
                    }
                }
            }
        ]
    },
        {
        "planetId": "2",
        "gameworldId": "1",
        "neighbours": [
            {
                "direction": "SOUTH",
                "id": "1"
            },
                        {
                "direction": "EAST",
                "id": "3"
            },

        ],
        "resourceType": null,
        "resource": null,
        "movementDifficulty": 0,
        "traits": [
            {
                "trait": "position",
                "context": "publicApi",
                "data": {
                    "position": {
                        "x": 0,
                        "y": 1,
                        "z": 0
                    }
                }
            }
        ]
    },
        {
        "planetId": "3",
        "gameworldId": "1",
        "neighbours": [
            {
                "direction": "WEST",
                "id": "2"
            },

        ],
        "resourceType": null,
        "resource": null,
        "movementDifficulty": 0,
        "traits": [
            {
                "trait": "position",
                "context": "publicApi",
                "data": {
                    "position": {
                        "x": 0,
                        "y": 0,
                        "z": 0
                    }
                }
            }
        ]
    },
        {
        "planetId": "4",
        "gameworldId": "1",
        "neighbours": [
            {
                "direction": "EAST",
                "id": "1"
            },

        ],
        "resourceType": null,
        "resource": null,
        "movementDifficulty": 0,
        "traits": [
            {
                "trait": "position",
                "context": "publicApi",
                "data": {
                    "position": {
                        "x": 0,
                        "y": 0,
                        "z": 0
                    }
                }
            }
        ]
    },

    
}

```
</details> 


## Layer + Position Trait

Simplified view of data using a local context by providing the layer Trait

![Layer + Position Trait](layer+position_trait.png)

<details> 
    <summary>Json</summary>

```json
{
    {
        "planetId": "1",
        "gameworldId": "1",
        "neighbours": [
            {
                "direction": "NORTH",
                "id": "2"
            },

        ],
        "resourceType": null,
        "resource": null,
        "movementDifficulty": 0,
        "traits": [
            {
                "trait": "position",
                "context": "publicApi",
                "data": {
                    "position": {
                        "x": 0,
                        "y": 0,
                        "z": 0
                    }
                }
            }
        ]
    },
        {
        "planetId": "2",
        "gameworldId": "1",
        "neighbours": [
            {
                "direction": "SOUTH",
                "id": "1"
            },
                        {
                "direction": "EAST",
                "id": "3"
            },

        ],
        "resourceType": null,
        "resource": null,
        "movementDifficulty": 0,
        "traits": [
            {
                "trait": "position",
                "context": "publicApi",
                "data": {
                    "position": {
                        "x": 0,
                        "y": 0,
                        "z": 0
                    }
                }
            }
                        {
                "trait": "layer",
                "data": {
                    "layer": ""
                }
            }
        ]
    },
        {
        "planetId": "3",
        "gameworldId": "1",
        "neighbours": [
            {
                "direction": "WEST",
                "id": "2"
            },

        ],
        "resourceType": null,
        "resource": null,
        "movementDifficulty": 0,
        "traits": [
            {
                "trait": "position",
                "context": "publicApi",
                "data": {
                    "position": {
                        "x": 0,
                        "y": 1,
                        "z": 0
                    }
                }
            },
            {
                "trait": "layer",
                "data": {
                    "layer": "1"
                }
            }
        ]
    },
        {
        "planetId": "4",
        "gameworldId": "1",
        "neighbours": [
            {
                "direction": "EAST",
                "id": "1"
            },

        ],
        "resourceType": null,
        "resource": null,
        "movementDifficulty": 0,
        "traits": [
            {
                "trait": "position",
                "context": "publicApi",
                "data": {
                    "position": {
                        "x": -1,
                        "y": 0,
                        "z": 0
                    }
                }
            },
            {
                "trait": "layer",
                "data": {
                    "layer": "2"
                }
            }
        ]
    },

    
}

```

</details>