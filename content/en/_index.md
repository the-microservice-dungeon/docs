---
title: The Microservice Dungeon
linkTitle: The Microservice Dungeon
---

{{< blocks/cover title="Welcome to The Microservice Dungeon" image_anchor="top" height="full" color="orange" >}}
<div class="mx-auto">
	<a class="btn btn-lg btn-primary mr-3 mb-4" href="/docs">
		Learn More <i class="fas fa-arrow-alt-circle-right ml-2"></i>
	</a>
	<a class="btn btn-lg btn-secondary mr-3 mb-4" href="https://gitlab.com/the-microservice-dungeon/">
		GitLab <i class="fab fa-gitlab ml-2 "></i>
	</a>
</div>
{{< /blocks/cover >}}


{{% blocks/lead color="primary" %}}
Welcome to The Microservice Dungeon Space Cadets 👩‍🚀

In a hidden corner of the universe lies the galaxy of Morpheus, an agglomerate of stars, planets, and asteroids filled to the brim with resources.
However, where there is wealth, there are people, and the thousands of robots that make Morpheus their home compete, trade, or fight for resources.
You're a space cadet and are an operator of robots, mining resources and expanding your fleet as you fend off pirates or invaders.
With no single correct path to victory, you can exert a commercial or military dominance over other crews, either alone or with a team.
Morpheus' wealth is yours for the taking, the only question is: How far are you willing to go?
{{% /blocks/lead %}}

{{< blocks/section color="dark" type="row" >}}

{{% blocks/feature title="Create robot swarms" image="images/feature_fight.png" %}}
You are a space cadet in the Galaxy of Morpheus. Create robots and enjoy your new adventure.
{{% /blocks/feature %}}


{{% blocks/feature title="Mine Resources" image="images/feature_galaxy.png" %}}
The planets are rich of expensive resources. Mine them and upgrade your fleet of robots. Beware of pirates!
{{% /blocks/feature %}}


{{% blocks/feature title="Engage in fights" image="images/feature_swarm.png" %}}
Will you be a pacifist or a warmonger. Fight other players in this battle of the galaxy.
{{% /blocks/feature %}}


{{< /blocks/section >}}
