baseURL: /docs/
title: The Microservice Dungeon

# cSpell:ignore goldmark github hugo readingtime docsy subdir lastmod pygments linenos catmullrom norsk gu

# Language settings
contentDir: content/en
defaultContentLanguage: en
defaultContentLanguageInSubdir: false
# Useful when translating.
enableMissingTranslationPlaceholders: true

enableRobotsTXT: true

# Will give values to .Lastmod etc.
enableGitInfo: true

# Comment out to enable taxonomies in Docsy
# disableKinds: [taxonomy, taxonomyTerm]

# You can add your own taxonomies
taxonomies:
  tag: tags
  category: categories

# Highlighting config
pygmentsCodeFences: true
pygmentsUseClasses: false
# Use the new Chroma Go highlighter in Hugo.
pygmentsUseClassic: false
# pygmentsOptions: "linenos=table"
# See https://help.farbox.com/pygments.html
pygmentsStyle: tango

# Configure how URLs look like per section.
permalinks:
  blog: /:section/:year/:month/:day/:slug/

# Image processing configuration.
imaging:
  resampleFilter: CatmullRom
  quality: 75
  anchor: smart

# Language configuration
languages:
  en:
    title: The Microservice Dungeon
    description: The Microservice Dungeon Documentation
    languageName: English
    weight: 1
  # cSpell:enable

markup:
  goldmark:
    parser:
      attribute:
        block: false
    renderer:
      unsafe: true
  highlight:
    # See a complete list of available styles at https://xyproto.github.io/splash/docs/all.html
    style: tango
    # Uncomment if you want your chosen highlight style used for code blocks without a specified language
    # guessSyntax: true
menu:
  main:
    - name: Architecture Decisions
      weight: 30
      url: https://the-microservice-dungeon.github.io/decisionlog/
      post: '<i class="pl-1 fas fa-external-link-alt fa-xs" aria-hidden="true"></i>'

# Everything below this are Site Params

# Comment out if you don't want the "print entire section" link enabled.
outputs:
  section: [HTML, print, RSS]

params:
  taxonomy:
    taxonomyCloud: []
    # taxonomyCloudTitle: [Tag Cloud, Categories]
    taxonomyPageHeader: []

  mermaid:
    version: 10.9.0

  privacy_policy: https://policies.google.com/privacy

  # First one is picked as the Twitter card image if not set on page.
  # images: [images/project-illustration.png]

  # Menu title if your navbar has a versions selector to access old versions of your site.
  # This menu appears only if you have at least one [params.versions] set.
  version_menu: Releases

  # Flag used in the "version-banner" partial to decide whether to display a
  # banner on every page indicating that this is an archived version of the docs.
  # Set this flag to "true" if you want to display the banner.
  archived_version: false

  # The version number for the version of the docs represented in this doc set.
  # Used in the "version-banner" partial to display a version number for the
  # current doc set.
  version: 0.0

  # A link to latest version of the docs. Used in the "version-banner" partial to
  # point people to the main doc site.
  url_latest_version: https://example.com

  # Repository configuration (URLs for in-page links to opening issues and suggesting changes)
  gitlab_repo: https://gitlab.com/the-microservice-dungeon/docs

  # An optional link to a related project repo. For example, the sibling repository where your product code lives.
  # github_project_repo: https://github.com/google/docsy

  # Specify a value here if your content directory is not in your repo's root directory
  # github_subdir: ""

  # Uncomment this if your GitHub repo does not have "main" as the default branch,
  # or specify a new value if you want to reference another branch in your GitHub links
  github_branch: main

  # Google Custom Search Engine ID. Remove or comment out to disable search.
  # gcs_engine_id: d72aa9b2712488cc3

  # Enable Lunr.js offline search
  offlineSearch: true

  # Enable syntax highlighting and copy buttons on code blocks with Prism
  prism_syntax_highlighting: false

  copyright:
    authors: Docsy Authors | [CC BY 4.0](https://creativecommons.org/licenses/by/4.0) |
    from_year: 2018

  # User interface configuration
  ui:
    # Set to true to disable breadcrumb navigation.
    breadcrumb_disable: false
    # Set to false if you don't want to display a logo (/assets/icons/logo.svg) in the top navbar
    navbar_logo: true
    # Not sure what that does
    footer_about_enable: true
    # Set to true if you don't want the top navbar to be translucent when over a `block/cover`, like on the homepage.
    navbar_translucent_over_cover_disable: false
    # Enable to show the side bar menu in its compact state.
    sidebar_menu_compact: true
    # Set to true to hide the sidebar search box (the top nav search box will still be displayed if search is enabled)
    sidebar_search_disable: false

    # Adds a H2 section titled "Feedback" to the bottom of each doc. The responses are sent to Google Analytics as events.
    # This feature depends on [services.googleAnalytics] and will be disabled if "services.googleAnalytics.id" is not set.
    # If you want this feature, but occasionally need to remove the "Feedback" section from a single page,
    # add "hide_feedback: true" to the page's front matter.
    feedback:
      enable: false
      # The responses that the user sees after clicking "yes" (the page was helpful) or "no" (the page was not helpful).
      'yes': >-
        Glad to hear it! Please <a href="https://github.com/USERNAME/REPOSITORY/issues/new">tell us how we can improve</a>.
      'no': >-
        Sorry to hear that. Please <a href="https://github.com/USERNAME/REPOSITORY/issues/new">tell us how we can improve</a>.

    # Adds a reading time to the top of each doc.
    # If you want this feature, but occasionally need to remove the Reading time from a single page,
    # add "hide_readingtime: true" to the page's front matter
    readingtime:
      enable: false

  links:
    # End user relevant links. These will show up on left side of footer and in the community page if you have one.
    user:
      - name: Archi-Lab
        url: https://www.archi-lab.io/
        icon: fa fa-globe
        desc: Archi-Lab
      - name: Discord
        url: https://discord.gg/YYNYb5whU8
        icon: fa-brands fa-discord
        desc: Archi-Lab Discord
    developer:
      - name: GitLab
        url: https://gitlab.com/the-microservice-dungeon
        icon: fab fa-gitlab
        desc: Development takes place here!
      - name: GitHub
        url: https://github.com/the-microservice-dungeon
        icon: fab fa-github
        desc: Legacy GitHub Organization!
  plantuml:
    enable: true
    theme: default
    svg_image_url: https://www.plantuml.com/plantuml/svg/
    svg: false

  markmap:
    enable: true

theme: docsy

frontmatter:
  lastmod: ["lastmod", ":git", ":fileModTime", "publishDate"]
